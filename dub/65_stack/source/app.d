import std.stdio;
import std.format;

void main(string[] args)
{
    // Create a simple stack using a dynamic array
    auto stack = new int[10];
    int top = -1;
    // Function to push an element onto the stack
    void push(int x) {
        if (top == 9) {
            writeln("Stack overflow");
            return;
        }
        stack[++top] = x;
    }
    // Function to pop an element from the stack
    int pop() {
        if (top == -1) {
            writeln("Stack underflow");
            return -1;
        }
        return stack[top--];
    }

    // Push some elements onto the stack
    push(10);
    push(20);
    push(30);
    // Pop and print the elements
    writeln(pop());
    writeln(pop());
    writeln(pop());
 } // main()


