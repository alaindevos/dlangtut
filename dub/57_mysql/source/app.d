import std.stdio:writeln;
import std.conv;
import std.array : array;
import mysql.safe; // Please use the safe api, it's the future

void main(string[] args)
{
	string test1="aaa";
	// Connect
	auto connectionStr = "host=127.0.0.1;port=3306;user=root;pwd=root;db=mysql";
	Connection conn = new Connection(connectionStr);
	scope(exit) conn.close();

	// Insert
	ulong rowsAffected = conn.exec(
		"INSERT INTO Names (firstname,lastname) VALUES ('aaa', 'bbb'), ('ccc', 'ddd')");
	// Query
	ResultRange range = conn.query("SELECT * FROM Names");
    MySQLVal firstname;
    MySQLVal lastname;
    string a;
    string b;
    foreach (row ; range){
        firstname = row[0];
        a=to!string(firstname);
        writeln(a);
        lastname = row[1];
        b=to!string(lastname);
        writeln(b);
    }
}
