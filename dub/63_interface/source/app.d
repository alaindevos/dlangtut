import std.stdio;
import std.format;
interface IVal {
    int getValue();
}

class CVal : IVal {
    private int value;

    public this(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
	}
}


void main(string[] args)
{
	writeln("Hello World ");
	CVal c=new CVal(5);
	int result=c.getValue();
	string sr=format("%d \n",result);
	write(sr);
} // main()


