import std.stdio:writeln;
import std.conv;
import std.array : array;
import dpq2;
import std.getopt;
import std.stdio: writeln;
import std.typecons: Nullable;
import vibe.data.bson;

void main(string[] args){
    string connInfo="postgres://x:x@127.0.0.1:5432/x";
    Connection conn = new Connection(connInfo);
    auto answer = conn.exec("SELECT * FROM person");
    for(int t = 0 ; t < answer.length ; t++ ){
        foreach(cell; rangify(answer[t])){
            writeln("Cell: ", cell.as!PGtext);
        }
    }
}
