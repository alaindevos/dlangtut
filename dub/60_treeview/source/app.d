import gtk.Main;
import gtk.MainWindow;
import gtk.Box;
import gtk.Button;
import gtk.Widget;
import gtk.ListStore;
import gtk.TreeViewColumn;
import gtk.CellRendererText;
import gtk.TreeView;
import gtk.TreeIter;
import gtk.TreeSelection;
import gtk.TreePath;
import gtk.TreeModelIF;
import gtk.TreeIter;
import std.stdio;
import gdk.Event;

class MyWindow : MainWindow {
	Box vbox;
	ListStore store;

   	void quit(){
		Main.quit();
	}
	
	void store_addrow(ref ListStore store,string name,int age){
		TreeIter it=store.createIter();
		store.setValue(it,0,name);
		store.setValue(it,1,age);
	}
	
	void view_addcolumn(ref TreeView tv,string name,int colindex){
		TreeViewColumn col=new TreeViewColumn();
		col.setTitle(name);
		col.setSortColumnId(colindex);
		tv.appendColumn(col);
		CellRendererText  r=new CellRendererText();
		//col.packStart(r,0);
		col.addAttribute(r,"text",colindex);
		r.setAlignment(1.0,1.0);
	}

	TreeIter store_get_row(int index){ 
			TreeIter it;
			store.getIterFirst(it);
			for(int t=0;t<index;t++){
				store.iterNext(it);
			}
			return it;
		}
		
	this(){
		super("Treeview");
		setSizeRequest(400,300);
		setHexpand(0);
		addOnDestroy(delegate void(Widget w) { quit(); } );
		vbox=new Box(Orientation.VERTICAL,5);
		auto mytreeview=new TreeView();
		
		store=new ListStore([GType.STRING,GType.INT]);
		store_addrow(store,"Alain.............................",10);
		store_addrow(store,"Eddy",5);

		TreeView tv=new TreeView();
		view_addcolumn(tv,"Name",0);
		view_addcolumn(tv,"Age",1);

		tv.setModel(store);

		vbox.packStart(tv,true,true,0);
		Button remove=new Button("Remove");
		remove.addOnButtonRelease(&removeme);
		vbox.packStart(remove,false,false,0);
		store_addrow(store,"Jan",15);
		add(vbox);
		showAll();
	}
	
	private bool removeme(Event event,Widget widget){
		writeln("Hallo");
		TreeIter it;
		bool result=true;
		//store.getIterFirst(it);
		//result=store.remove(it);
		store.clear();
		return true;
	}
	
}

int main(string[] args){
	Main.init(args);
	MyWindow window=new MyWindow();
	Main.run();
	return 0;
}
