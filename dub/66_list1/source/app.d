import std.stdio;
import std.format;

struct Node {
    int data;
    Node* next;
    static Node* last=null;
    void addNode(int x){
        writeln(format("adding node value : %d ",x));
        auto newNode = new Node(data: x, next: null);
        next = newNode;
        last = newNode;
    }
}

void main(string[] args)
{
    // Create nodes
    auto head = new Node(data: 10, next: null);
    head.addNode(20);
    (*Node.last).addNode(30);
    (*Node.last).addNode(40);

    // Print the linked list
    auto current = head;
    while (current != null){
        auto s=format("Current node value : %d ",current.data);
        writeln(s);
        current = current.next;
    }
}  // main()
