import std.stdio : writeln;
import std.conv;
import std.array : array;
import mysql.safe;
import std.stdio;
import gtk.MainWindow;
import gtk.Main;
import gtk.Widget;
import gtk.Label;
import gtk.Grid;

void myxy(Grid grid, string text, int x, int y)
{
    Label label = new Label(text);
    grid.attach(label, x, y, 1, 1);
}

void addtogrid(Grid grid)
{
    auto connectionStr = "host=127.0.0.1;port=3306;user=root;pwd=root;db=mysql";
    Connection conn = new Connection(connectionStr);
    scope (exit)
        conn.close();
    // Query
    ResultRange range = conn.query("SELECT * FROM Names");
    MySQLVal firstnameval;
    MySQLVal lastnameval;
    string firstname;
    string lastname;
    int y = 0;
    foreach (row; range)
    {
        firstnameval = row[0];
        firstname = to!string(firstnameval);
        myxy(grid, firstname, 0, y);
        lastnameval = row[1];
        lastname = to!string(lastnameval);
        myxy(grid, firstname, 1, y);
        y = y + 1;
    }
}

class MyWindow : MainWindow
{
    string title = "MyTitle";
    string byeBye = "Bye-bye";
    string message = "Greeting";
    this()
    {
        super(title);
        addOnDestroy(&quitApp);

        Grid grid = new Grid();
        addtogrid(grid);
        add(grid);

        this.setSizeRequest(600, 600);
        showAll();
        greeting();
    } // this()
    void quitApp(Widget widget)
    {
        writeln(byeBye);
        Main.quit();
    } // quitApp()
    void greeting()
    {
        writeln(message);
    } // greeting()
} // class TestRigWindow
void main(string[] args)
{
    Main.init(args);
    MyWindow myWindow = new MyWindow();
    Main.run();
} // main()
