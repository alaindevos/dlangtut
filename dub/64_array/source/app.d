import std.stdio;
import std.format;

void main(string[] args)
{
	writeln("Hello World ");
    int[4][6] a;
    for( int y = 0; y < 4; y = y + 1 ) {
        for( int x = 0; x < 6; x = x + 1 ) {
            a[x][y]=x*y;
        }
    }
    for( int y = 0; y < 4; y = y + 1 ) {
        for( int x = 0; x < 6; x = x + 1 ) {
            write(format(" || %d %d %d ||",x,y,a[x][y]));
        }
        writeln("");
    }
} // main()


