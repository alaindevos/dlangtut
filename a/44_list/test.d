import std.stdio: write,writeln;
import std.range: empty,popFront,front;

struct Node {
	int element;
	Node * next;
}

void print(const(Node) * list) {
	for ( ; list; list = list.next) {
		write(' ', list.element);
	}	
}

struct List {
	this(int[] AR)
		{foreach(i ; AR)pushfront(i);} 
	Node * root=null;
	bool empty() const {return !root;}
	void popFront() {root=root.next;}
	float front() const {return root.element;}
	void pushfront(int element) {
		Node * newnode=new Node();
		newnode.element=element;
		newnode.next=root;
		root=newnode;
	}
	void printall(){
		Node *l=root;
		for( ; l ; l=l.next){
			writeln(l.element);
			}//for
		}//printall
}//List

void main(){
	//List * l=new List();
	//l.pushfront(2);
	//l.pushfront(1);
	List *l=new List([3,2,1]);
	foreach(element; *l) writeln(element);
	foreach(element; *l) writeln(element);
	(*l).printall();
	(*l).writeln();
	print((*l).root);
}
