import std.stdio: writeln;
import std.range: Recurrence,recurrence,take;
const string f="a[n-1] + a[n-2]";
void main() { auto r=recurrence!(f)(1, 1); auto t=r.take(42); writeln(t);}
