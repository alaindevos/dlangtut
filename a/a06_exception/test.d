import std.stdio:writeln;

class MyException:Exception {
	this(string message,
		 string file=__FILE__,
		 size_t line=__LINE__,
		 Throwable next=null){
		super(message,file,line,next);
	}
}

void main(){
	try
		throw new MyException("mymessage");
	catch(MyException e)
		writeln("Caught:",e);
}
