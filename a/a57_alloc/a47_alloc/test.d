import std.stdio:writefln;
import object: destroy;
import core.memory: GC;
import core.stdc.stdlib: malloc,free;


void dofun(){
   auto pa=cast(int *)malloc(1000*int.sizeof);
   writefln("%12x",pa);
   auto a=pa[0..1000];
   free(a.ptr);
}

int main(){
    dofun();
   auto pb=cast(int *)malloc(1000*int.sizeof);
   writefln("%12x",pb);
   auto b=pb[0..1000];
   free(b.ptr);
   return 0;

}
