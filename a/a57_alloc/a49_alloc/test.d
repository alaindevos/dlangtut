import std.stdio:writefln;
import object: destroy;
import core.memory: GC;
import core.stdc.stdlib: malloc,free;
import std.typecons;

struct S {
     int * pa;
     int [] a;
   
	this(int dummy)  {
         writefln("Called constructor");
         pa=cast(int *)malloc(1000*int.sizeof);
         a=pa[0..1000];
         }
	
   ~this(){
      writefln("Called Destructor");
      free(a.ptr);
   }
	
}

void dofun()
{
   S s=S(0);
   (s.a)[3]=5;
   writefln("%12x",&s);

}
int main(){
   dofun();
   dofun();
   return 0;

}
