
extern(C) @nogc nothrow {
	void libprintme(char *s);
    alias pprintme=void function(char *s);
    __gshared pprintme cprintme=&libprintme;

}

extern(D) {
	void dprintme(string ds){
		alias dstring=string;
		alias cstring=char *;
		import std.conv: to,castFrom;
		cstring cs=castFrom!dstring.to!cstring(ds);
		(*cprintme)(cs);
		import core.stdc.stdio: printf;
		printf("World");
	}

	// In D

	void main(){dprintme("Hello\n");



	void sum(const(int)* array, int n){}
	int[16] coeff;
	sum(coeff.ptr, coeff.sizeof / int.sizeof); // array not implicitely convertible to a pointer

}//main
}//externD

