//Unit test is used to ensure that code works as expected. 
//Debug is used when you need to find why the code doesn't work as expected.
//Unit tests before bug fixes

import std.stdio: writeln;

int fibo(int n)
in
    {//Contract precondition ,disabled in release version
        assert(n>=0,"debug Fibonacci parameter must be positive"); //Runtime check debug version
    } 
out (result) 
    {//Contract postcondition disabled in release version
        assert(result>=0,"debug Fibonacci result must be positive"); //Runtime check debug version
    } 
do  {//Body
		debug 
			{ // Only Debugcode 
			}
		else  
			{ // Only Releasecode 
			}
		if(n>1)
            return (fibo(n-1)+fibo(n-2));
        else
            return n;
       }


static assert (fibo(3)==2,"debug static assert,Fibo 3 must be 2"); //Compile time check debug version (not frequently used)

unittest {// Executed during development of the program , unittest version
            static assert (fibo(3)==2,"unittest static assert,Fibo 3 must be 2"); //Compile time check
         }

unittest {// Executed during development of the program , unittest version
            assert (fibo(3)==2,"unittest assert,Fibo 3 must be 2");               //Runtime check
         };


void main(){
writeln(fibo(3));
writeln(fibo(42));
writeln(fibo(-42));
}
