import std.stdio:writeln;

abstract class Shape {
	abstract double get_Surface();
	abstract double get_Circumference();
}

class Rectangle : Shape  {
	private double length;
	private double width;
	
	override double get_Surface(){ return length*width;}
	override double get_Circumference(){ return 2*(length+width);}
	
	this(double length,double width){
		this.length=length;
		this.width=width;
	}
}

class Square : Rectangle {
	this(double size){
		super(size,size);
	}
}

int main(){
Rectangle r=new Rectangle(2,4);
writeln(r.get_Surface);
writeln(r.get_Circumference);
Square s=new Square(3);
writeln(s.get_Surface);
writeln(s.get_Circumference);
return 0;
}
