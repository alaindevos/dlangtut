import std.stdio;

void testimmutable() @safe 
{
immutable int x=10;
//int *px=cast(int*) (&x);    //not allowed with @safe
//*px=9;

writeln(x);
}



void main() @trusted  // drop check , //@safe   // checks error
{
testimmutable();

int *p=null;
void myfun(){
	int x=2;
	p=&x;
	writeln(p);
	writeln(x);
}
myfun();
*p=16;
writeln(p);
writeln(*p);
}
