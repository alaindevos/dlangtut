/++
Example is a structure meant to demonstrate Ddoc documentation.
In addition to the first one-line short description, we can also
write
longer multiline descriptions to expand on the object's
functionality.
+/
struct Example {
	int a; /// This integer is documented briefly.
/**
This function has documented parameters.
We document all the parameters in a params section.
The return value and exceptions can also be documented.
Params:
x = The x coordinate
Returns:
	The y coordinate
Throws:
Exception if x is negative.
*/
	int getY(int x) { return 0;
//*implementation irrelevant */ }
	}
}

void main(){}