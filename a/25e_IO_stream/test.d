import std.stdio:write,writeln,readln;
import std.format:formattedRead;
import std.string:strip;
 
void main(){

	//standard input/output streams---------------------------------------
	// ---- MORE USER INPUT ----
	// readln reads until the end of the line
	// and stores input in an array (Boxes in memory
	// for each character)
	// char[] arrName;
	// write("What's your name? ");
	// readln(arrName);
	// // strip deletes the newline at the end of array
	// writeln("Hello ", strip(arrName));
 
	// // Read in multiple values at once
	// write("Enter 1st & last name? ");
	// string fName = strip(readln());
	// string fN, lN;
	// // Define the input you expect 2 strings
	// // separated by a space
	// formattedRead(fName, " %s %s",
	//   fN, lN);
	// writeln("Hello ", fN, " ", lN);

	writeln("Hello World");
	char[] name1;
	write("What is your name?:");
	readln(name1);
	name1=name1.strip;
	writeln("Your name is:",name1);
	write("Please enter name and age:");
	string line=readln().strip;
	string name;
	int age;
	formattedRead(line,"%s %s",name,age);
	writeln("Your name is ",name," Age ",age);

}
