void main(){
     int[] i=new int[10000];
     import object: destroy;
     destroy(i);
     import core.memory: GC;
     assert(i.ptr is null); // yep
     GC.free(i.ptr); // basically free(null) which is a no-op
}