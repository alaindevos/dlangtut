import std.stdio:writefln;
import object: destroy;
import core.memory: GC;

void dofun(){
    auto a=new int[1000];
    writefln("%12x",&a);
    destroy(a);
    GC.free(a.ptr);
}

int main(){
    dofun();
    auto b=new int[1000];
    writefln("%12x",&b);
    return 0;
}
