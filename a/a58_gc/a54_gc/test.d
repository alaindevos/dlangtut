import std.stdio:writeln;
import core.memory: GC;
void main(){
	GC.enable();
	GC.collect();
	int *a=new int;
	writeln(GC.stats.freeSize);
	writeln(GC.stats.usedSize);
    int *b=cast(int*)GC.calloc(int.sizeof);
	writeln(GC.stats.freeSize);
	writeln(GC.stats.usedSize);
    GC.free(b);
	writeln(GC.stats.freeSize);
	writeln(GC.stats.usedSize);
}
