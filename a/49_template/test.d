template HasUniqueElements(int[] arr)
{
    import std.algorithm : sort, uniq;
    import std.array : array;
    enum bool HasUniqueElements = arr.sort().uniq.array.length == arr.length;
}

static assert(HasUniqueElements!( [5, 2, 4] ));
static assert(!HasUniqueElements!( [1, 2, 3, 2] ));

void main(){
}