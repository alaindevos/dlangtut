import vibe.d;
import std.conv : to;

struct Name
{
    string firstname;
    string lastname;
}

Name[] names;

final class WebChat
{
    // GET /
    void get()
    {
        render!("index.dt", names);
    }

    void getEdit(string myid)
    {
        int id = myid.to!int;
        string firstname = names[id].firstname;
        string lastname = names[id].lastname;
        render!("edit.dt", id, firstname, lastname);
    }

    void postEdit(string myid, string firstname, string lastname)
    {
        int id = myid.to!int;
        names[id].firstname = firstname;
        names[id].lastname = lastname;
        render!("index.dt", names);
    }

    void postAdd(string firstname, string lastname)
    {
        names ~= Name(firstname, lastname);
        render!("index.dt", names);
    }

    void getDelete(string myid)
    {
        int id = myid.to!int;
        names = names[0 .. id] ~ names[(id + 1) .. names.length];
        render!("index.dt", names);
    }
}

void main()
{

    names ~= Name("Alain", "Devos");
    names ~= Name("Eddy", "Dewolf");
    names ~= Name("Jan", "Dehond");
    names ~= Name("Piet", "Dekat");

    // the router will match incoming HTTP requests to the proper routes
    auto router = new URLRouter;
    // registers each method of WebChat in the router
    router.registerWebInterface(new WebChat);
    // match incoming requests to files in the public/ folder
    router.get("*", serveStaticFiles("public/"));

    auto settings = new HTTPServerSettings;
    settings.port = 8080;
    settings.bindAddresses = ["::1", "127.0.0.1"];
    // for production installations, the error stack trace option should
    // stay disabled, because it can leak internal address information to
    // an attacker. However, we'll let keep it enabled during development
    // as a convenient debugging facility.
    //settings.options &= ~HTTPServerOption.errorStackTraces;
    listenHTTP(settings, router);
    logInfo("Please open http://127.0.0.1:8080/ in your browser.");

    runApplication();
}
