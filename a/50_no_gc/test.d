void main() {
    import core.memory;
    import std.stdio;
    GC.disable;
    writeln("Goodbye, GC!");
    //No more freeing of memory
    int []a=[1,2,3,4];
    int * b=new int;
}