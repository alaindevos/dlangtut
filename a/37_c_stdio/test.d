import core.stdc.stdio: printf;
import std.conv: to,castFrom;

void dprintf(string ds){
	alias dstring=string;
	alias cstring=const char *;
	alias cprintf=printf;
	auto  cs=castFrom!dstring.to!cstring(ds);
	cprintf("%s",cs);
}

void main(){
	dprintf("Hello World\n");
}
