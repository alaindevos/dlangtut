import std.stdio;

class X {
	double x;
	public void intx(int x)
		{this.x=x;}
	protected void doublex(double x)
		{this.x=x;}
}// X

class Child : X {
	double y;
	override void intx(int x)
		{super.intx(x);
		 this.y=x;
		} //intx
	override void doublex(double x)
		{
		 super.doublex(x);
		 this.y=x;
		} //doublex
	}//Child
	
void main(){
Child c=new Child();
c.doublex(5.0);
}
