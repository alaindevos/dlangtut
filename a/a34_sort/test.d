import std.stdio: writeln;
import std.algorithm:sort;
import std.algorithm.comparison:cmp;

void main(){
	///sort ints
	int[] d = [1,2,3];
	auto s1 = sort!((a,b)=> a>b)(d);
	///sort strings
    string[] s=["b","c","a"];
	auto s2 = sort!((a,b)=>cmp(a,b)<0)(s);
}
