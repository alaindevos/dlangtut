import std.stdio: write,writeln;
import std.range: empty,popFront,front;

//popFront : removes first element from variable length array

void print(T)(T range) {
    for ( ; !range.empty; range.popFront()) {
        write(' ', range.front);
    }
    writeln();
}

void main() {
    int[] array = [ 1, 2, 3, 4 ];
    print(array);
}

