//Generic is type independent-code

import std.array;
import std.stdio: writeln;

bool binarySearch(T)(T[] input, T value) {
	while (!input.empty) {
		auto i = input.length / 2;
		auto mid = input[i];
		if	(mid > value) input = input[0 .. i];
		else if (mid < value) input = input[i + 1 .. $];
		else return true;
	}
return false;
}

unittest {
	assert(binarySearch([1,3,6,7,9,15],6));
	assert(!binarySearch([1,3,6,7,9,15],5));
}


void main(){

	//generics,templates
    // ---- TEMPLATES ----
    // Templates allow you to define algorithms
    // that work with multiple data types
    void getSum4(T)(T x, T y){
      writeln(x, " + ", y, " = ", x + y);
    }
    getSum4(4,6);
    getSum4(4.5, 5.6);
    // You can use templates any place you define types
    // You can define a template block of code
    template tempSamp(T){
      struct Shape{
        T height;
        T width;
      }
      T getArea(Shape shape){
        return shape.height * shape.width;
      }
    }
    auto shape = tempSamp!int.Shape(4,5);
    writeln("Area : ",
    tempSamp!int.getArea(shape));
    // ---- MIXINS ----
    // Mixins allow you to generate code
    // at run time
    // Generate int var1 = 10;
    mixin MakeType!(int, 10);
    writeln(var1);
    // Generate a function
    mixin GetSum!(double);
    writeln("1.2 + 2.3 = ",
    add(1.2,2.3));

 

// ---- MIXINS ----
mixin template MakeType(T, T x){
  T var1 = x;
}
 
mixin template GetSum(T){
  T add(T x, T y){
    return x + y;
  }
}

	
//Template
template StaticArray(Type, size_t Length) {
     class StaticArray {
          Type[Length] content;
          
          size_t myLength() {
            return getLength(this);
          }
     }
     private size_t getLength(StaticArray arr) {
          return Length;
     }
}
StaticArray!(int, 5) arr5 = new StaticArray!(int, 5);
writeln(arr5.myLength());
//Function template
T min(T)(in T arg1, in T arg2) {
    return arg1 < arg2 ? arg1 : arg2;
}
//Automatic type inference
writeln(min(1, 2));
//Explicit type
writeln(min!(ubyte)(1, 2));
//With single type, the parenthesis might be ommited
writeln(min!ubyte(1, 2));

template Template(T){
	T v;
	void printme(){
		import std.stdio:writeln;
		writeln("The type is:",typeid(T));
		writeln("The value is",v); 
	}//printme
}//template

	//Intantiate template, create code
	Template!(int).v=20;
	Template!(int).printme();  //20
	
	//Only one parameter , no ()
	alias mtf=Template!float;
	mtf.printme();             //Not a number

//-----------------------------------------------------------------	
template Template2(T) {
	struct Wrapper {
		T val;
		void printme() {
			import std.stdio : writeln;
			writeln("The type is ", typeid(T));
			writeln("The value is ", val);
		}//printme
	}//struct
}//template

	//Intantiate template
	Template2!int.Wrapper vw1;
	//Intantiate nothing new, declare a new Wrapper.
	Template2!int.Wrapper vw2;
	vw1.val=10;
	vw2.val=20;
	vw1.printme();//10
	vw2.printme();//20
	
//Eponymous template-----------------------------------------------	
template Wrapper(T) {
	struct Wrapper {
		T val;
		void printme() {
			import std.stdio : writeln;
			writeln("The type is ", typeid(T));
			writeln("The value is ", val);
		}//printval
	}//struct
}//template
	//Intantiate template, short notation
	//In fact : Wrapper!int.Wrapper w0;
	//Shorter notation
	Wrapper!int w1;
	
//Shortcut "struct-template" declaration-------------------------------
struct Wrapper2(T) {
	T val;
	void printme() {
		import std.stdio : writeln;
		writeln("The type is ", typeid(T));
		writeln("The value is ", val);
	}//printval
}//template
	Wrapper2!int w2;
	alias iwrapper=Wrapper2!int;
	iwrapper w3;
	
}
