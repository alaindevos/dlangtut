//Imperative:Yes
//Object-oriented:Yes
//Functional:Yes
//Procedural:Yes
//Generic:Yes
//Reflective:Yes
//Event-Driven:No
//Other:Generative,Concurrent
//Standerdized:No

//Type safety:Almost Safe
//Type expression:Explicit
//Type compatibility and equivalence:Nominal
//Type checking:Static

//Failsafe I/O, Throwing on failure


//Libraries
import std.stdio;

void main()
{
	//Line continuation  : \\

/+
	// A single line of comment
	/*
		A comment that spans
		multiple lines
	*/
+/
 
//statement : ended by ; or block {}----------------------

//Statement ended by semicolon
	int a=0;
	a=a+a;
	writeln("Starting"); //Starting

//Block
	{
		int b=0;
		int c=b+b;
	}
    
//Infix expression, Assignment
	a= (1+2)*(3+4) ;
}
