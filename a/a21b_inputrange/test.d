import std.stdio: write,writeln;
import std.range: empty,popFront,front;
import std.typecons: Tuple;

void print(T)(T range) {
    for ( ; !range.empty; range.popFront()) {
        write(' ', range.front);
    }
    writeln();
}


alias Student = Tuple!(string, "name", int, "number");

struct School {
    Student[] students;
}//School

struct StudentRange {

    Student[] students;

    this(School school) {
        this.students = school.students;
        }//this

    bool empty() const {
        return students.length == 0;
        }//empty

    ref Student front() {
        return students[0];
        }//front

    void popFront() {
        students = students[1 .. $];
        }//popFront


    StudentRange studentsOf(ref School school) { //Convenience function
        return StudentRange(school);
        }

    void swapFront(ref StudentRange x){ //Convenience function
        Student s=x.students[0];
        x.students[0]=this.students[0];
        this.students[0]=s;
        }

}//StudentRange

void swapFront(ref StudentRange x,ref StudentRange y){x.swapFront(y);}

void main(){
    auto turkishSchool = School( [ Student("Ebru", 1),Student("Derya", 2) ,Student("Damla", 3) ] );
    auto americanSchool = School( [ Student("Mary", 10),Student("Jane", 20) ] );
    auto turkishSchoolrange=StudentRange(turkishSchool);    
    auto americanSchoolrange=StudentRange(americanSchool);
    swapFront(turkishSchoolrange,americanSchoolrange);
    print(turkishSchoolrange);
    print(americanSchoolrange);
}//main

