// Exception is a subclass of Throwable
// Error is a subclass of Throwable
// Rule : Don't catch Error or Throwable, only Exception
// Compile with "-g" for backtraces , symbolic debuginfo

import std.stdio: writeln,StdioException;

void main(){

// ---- EXCEPTIONS ----
// Through exception handling you can
// block potential errors. You wouldn't
// normally handle divide by zero errors
// this way, but this is an easy example
// to understand
// Wrap problematic code with try
try{
	int zero = 0;
	// assert can be used to test assumptions
	// during the development process
	// assert(zero != 0, "Can't be zero");
	if (zero == 0){
		// Throw error to be handled
		throw new Exception("Can't divide by zero");
		} else {
		int badInt = 10 / zero;
		}
	}
	// Will catch any exception as well as
	// yours and print a message rather than
	// crashing
	catch(Exception e){
		writeln(e.msg);
	}
// Close files, databases, etc. here
finally{
	writeln("Cleaning up");
}

void fun(){
	throw new Exception("My Exception");
}

writeln("Exception TEST1");
try{
	fun();
}//try
catch(Exception e){
	writeln(e);
}//catch

//-----------------------------------------------------
writeln("Exception TEST2");
class MyException : Exception{
	this(string s){super(s);}
}

void func(int x){
	if(x==1) { throw new    MyException("MyException");}
		else { throw new StdioException("Stdioexception");}
		
}

try{
	func(0);
}//try
catch(StdioException e){
	writeln("Stdioexception");
}//catch
catch(MyException e){
	writeln("Myexception");
}

try{
	func(1);
}//try
catch(StdioException e){
	writeln("Stdioexception");
}//catch
catch(MyException e){
	writeln("Myexception");
}
// always do finally
finally{
	writeln("finally");
}//finally

//Don't throw an exception
int dontthrow(int a,int b) nothrow {
	return a/b;
}

}//main
