//Multiple inheritence is not supported
//Classes can only inherit from one other class
import std.stdio:writeln;
import std.typecons: scoped;
import core.stdc.stdlib: malloc,free;
//import std.typecons;

class Buffer {
	private void* data;
	// Constructor
	this() {data = malloc(1024);}
	// Destructor
	~this() {free(data);}
	
}


//Abstract class
class Shape{
	int x;
	abstract void draw();
}

struct S {
	private int _x;
	void opAssign(S s) { _x = 5*s._x; } // OK
	void opAssign(int x) { _x = 7*x; } // OK
}

class C {
	private int _x;

	// Error: Identity assignment overload is illegal, i.e. reference type !!!
	// They would need to be the same.
	// void opAssign(C c) { _x = c._x; }

	void opAssign(int x) { _x = 3*x; } // OK
}

// superclass / parent class / base class
// Note : A class allocates memory on the heap (not the stack)
class IncrementalStat {
	
	// "private" : Symbol acces is restricted to this file,module!!!!!!!
	// For class level protection put class in own file !!!!!!!!!!!!!!!
	private int data;
	
	//"package": symbol access to this directory!!!!!!!!!!!!!!!!!!!!!!!!
	package int date2=5;

	// "protected" : access to this file plus all children!!!!!!!!!!!!!!
	protected int data3;        
	protected double result;
	
	//  "public" : access to it from all imports !!!!!!!!!!!!!!!!!!!!!!!
	public int data4; 
	
	
	abstract void accumulate(double x);
	void postprocess() {writeln("Incremental-postproces");}
	double fresult(){
		return result;
	}
	
	//Private function is final and cannot be overridden !!!!!
	private int myfunx(int x){return x;}
		
}

// subclass / child class /derived classs
class Average : IncrementalStat {
	private uint items=0;
	enum aaa=0.2; 
	static x="bbb";


	//Forwarding constructor
	this(int a,int b){this(a*b);}

	//Constructor;
	this(int items) { this.items=items;x="ccc";}

	//Constructor
	this() {result=0;this(3);}
	
	
	override void accumulate(double x){
		result+=x;
		++items;
	}
	override void postprocess(){
		super.postprocess(); // Call the overrriden method
		super.data=5;        // Access parentdata
		IncrementalStat.data=5 ; //Access parendata
		writeln("Average-postproces");
		if(items){
			result=result/items;
		}
	}
	
	// non-overridable method
	final test(){}
	
	static sayhi(){writeln("Hi");}
	
	//Destructor. In practice class destructors are run
	//on termination of the program in any order !!!!!
	//Destructor should never manipulate stack memory only heap memory!!!!

	int a;
	~this()
		{writeln("I'm free");
		//a=5;              //Don't do this !!!!
		//free(my_new_pointer) // OK
		}
		
	//Static constructor
	static this(){//
		}	
	//Static destructor
	static ~this(){//
		}
		
	
}


class MyClassX{}

void myfun(IncrementalStat i){}

void main(){
	
	// Allocate a class object on stack with std.typecons.scoped
	//scoped replaces new and put a class object on the stack, with the double benefit of avoiding GC and performing deterministic destruction.
	// no need for 'new', and automatic destructor call at scope exit.
	auto myClassX = scoped!MyClassX(); 

	auto b=new Buffer;
	
	//Most basic class
	class MySimpleton {}

	//static variable, only one of it exists;
	class CCC {
		static int i;
		}
	CCC.i=0;
		
	//static function , has no this
	class CCCC {
		static void doit(){writeln("Hallo");}
	}
	CCCC.doit();//Hallo


	Average x=new Average;
	x.sayhi();//Hi
	Average.sayhi();//Hi

	//y has type Incrementalstat be really refers to an Average !!!!!!
	//y has static type Incrementalstat and dynamic type Average
	IncrementalStat y=x;// assign child to parent, use subclass in place of superclass
    writeln("-");
	y.postprocess(); //!!!!! Calls average-postprocess of child
                     //Prints: Incremental-postprocess
                     //Prints: Average-postprocess
    writeln("+");
	myfun(y);
    writeln("/");
	myfun(x);
    writeln("*");
	writeln(x.toString()); // Textual representation ie. test.Average
	auto z=Object.factory("test.Average");
	
	//Final class cannot be extended
	final class FFF{int x;}
	
	IncrementalStat i1;
	IncrementalStat i2=i1; //Points to the same object
	//The "copy" postblit function this(this) is not available for classes
	
	class  T{
			char []s;
			this(const char[] s){
				this.s=s.dup;
			}//this
			
	}//class

	T t=new T("");
	auto aaa=typeid(t);
	auto bbb=typeid(typeof(t));

	S s1, s2;
	s1 = 1;      //*7
	s2 = s1;     //*5 , !!! ALLOWED.
	writeln(s2); //S(35);
	C c = new C;
	c = 10;      //*3
	writeln(c._x); //30 
	
	struct Matrix3 {
		double[3][3] values;
		
		// Returns a reference to this, annotate with return
		ref double opIndex(size_t i, size_t j)  return {
			return values[i][j];
			}
		}
		
	class MyX{int x;
		int result(){return x;}
		}
		
	//Scoped
	// scoped() wraps the class object inside a struct and the destructor 
	//of that struct object destroys the class object when itself goes out 
	//of scope.The effect of scoped() is to make class objects behave 
	//similar to struct objects regarding lifetimes.
		{
			auto p=scoped!MyX();
            writeln("A");
			p.result().writeln;//0
            writeln("B");
		}
	//I'm free
    //I'm free
}

