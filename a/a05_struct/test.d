import std.stdio:writeln;
import std.math:sqrt;
struct SVector {
	float[2] startpoint;
	float[2] endpoint;
	@disable this();
	this(int len){
		startpoint[0]=0;
		startpoint[1]=0;
		endpoint[0]=len;
		endpoint[1]=0;
	}

	float len(){
		float difx=endpoint[0]-startpoint[0];
		float dify=endpoint[1]-startpoint[1];
		float r2=difx*difx+dify*dify;
		float r=sqrt(r2);
		return r;
	}
}

void main(){
	SVector sv=SVector(5);
	float l=sv.len();
	writeln(l);
}
