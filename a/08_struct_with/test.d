import std.math;
import std.stdio:writeln,writefln;

//STRUCT-------------------------------------------------------------------
struct point{
	double x,y;
	double norm() {return sqrt(x*x+y*y);}
}      // no ; after struct


void main(){

	// ---- STRUCTS ----
	// Structs are custom types that
	// combine other types of data
	
	struct MyStruct2
	{
    	this(int dummy)
    	{
    	}
    	
    	MyStruct2 opAssign(int i){return this;}
	}

	void myFunction(MyStruct2 a){}

	// This is explicit constructor call
	MyStruct2 a2 = MyStruct2(0);     

	// This is an implicit constructor call
	MyStruct2 b2 = 1;               

	// Blit, and eventually postblit call if it is defined
	MyStruct2 c2 = b2;           

	// Same as previous line, parameters created from existing structs
	myFunction(c2);                         

	// Implicit call to `opAssign` with an `int` parameter, fails if it doesn't exist
	a2 = 2;                        

	// Implicit call to default opAssign, or a custom one with a `MyStruct` parameter
	b2 = a2;

	// Same as previous line
	// Except a temporary instance is created and destroyed on that line
	c2 = MyStruct2(3);


	struct MyAggregateStruct
	{
    	// `MyStruct(0)` evaluated at CTFE, 
    	// not during MyAggregateStruct construction!
    	MyStruct2 a2 = MyStruct2(0); 
	}
	
	
	struct Customer{
		string name;
		string phone;
		double bal = 0;
		// Static variables are part of the struct
		static numCusts = 0;
	}
	// Create and add data
	Customer c5;
	c5.name = "Paul Smith";
	c5.phone = "555-1212";
	c5.bal = 340.10;
	Customer.numCusts++;
	writefln("Call %s at %s about $%.2f", c5.name, c5.phone, c5.bal);//Call Paul Smith at 555-1212 about $340.10
	// Create on one line
	Customer c6 = {"Sue Smith", "555-1213", 18.90};
	Customer.numCusts++;
	writeln("Number of custs : ",Customer.numCusts);//2

	//Struct created on the stack , no inheritance for structs (vs class)
	struct integer { int x;}

	struct pair{
		int x,y;
		this(int val){ x=y=val;}
		//Disable default constructor
		@disable this();
		
		//Destructor is reliable called as soon as object goes out of scope
		~this(){writeln("Destructor");}

static ~this(){writeln("Static Destructor");}		
		
 	}
 	
 	pair ppp=pair(2);

	//WITH, uses namespace
	point p;
	int z;
	with(p) {
		x=3;
		p.y=4;
		z=1;
	}

	//NESTED-STRUCT--------------------------------
    int i = 7;
    struct SS {
        int x;
        int bar() { return x + i + 1; }
    }
    SS s;
    s.x = 3;
    writeln(s.bar());//11

	//Structs-vs-classes---------------------------
	//Struct created on the stack
	struct MyStruct {
		int data;
		int i=65 ; // Constructor
	}

	class MyClass {
		int data;
	}

	//Structs are value types
	MyStruct s1;
	MyStruct s2=s1;
	++(s2.data);
	writeln(s1.data);//0
	
	//Classes are reference types
	MyClass c1=new MyClass;
	MyClass c9=c1;
	++(c9.data);
	writeln(c1.data);//1
	
	MyClass c3=new MyClass;
	c3.data=2;
	MyClass * pcs= & c3;
	writeln((*pcs).data);//2
	
	//Static variable, only one of it exists
	struct S {
		static int si=3;
	}
	S.si=2;
	
	//Static function, has no this parameter
	struct S2{
		static void fun(){writeln("Hallo");}
		}
	S2.fun();//Hallo
	
	
	//Pass by refefence
	void fun(ref MyStruct s){
		s.data=5;
	}

	//Field oriented contructor
	MyStruct s3=MyStruct(1,2);
	fun(s3);

	//Shallow copy
	struct Shallow {
		int[] arr;
	}
	
	void printptr(Shallow i){
		writeln(i.arr.ptr);
	}

	Shallow a=Shallow([10,20,30]);
	writeln("Outer pointer ",a.arr.ptr);  //801000010
	writeln("Inner");
	printptr(a);      // 801000010, same address

	//Postblit constructor against shallow-copy-problem
	//Blit=Block transfer = raw memory copying
	struct Widget {
		private int[] array;
		// Postblit constructor
		this(uint length) {
		array = new int[length];
			}
		// Postblit constructor !!!!!!!!!!!!!!!!!!!!!!!!!!!
		this (this ) {
			int[] t= array.dup;
			array = t;
			}
		int get(size_t offset) { return array[offset]; }
        void set(size_t offset, int value) { array[offset] = value; }
        //Assignment operator!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //RETURN ANNOTATION !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        ref Widget opAssign(ref Widget rhs) return
			{
			if(array.length<rhs.array.length){
				array=rhs.array.dup;
				}
			else {
				array.length=rhs.array.length;
				//Copy array contents
				array[]=rhs.array[];
				}//else
			return this;
			}//opsassign
               
	}//widget
	
	//Offsets
	struct A {
		char a;
		int b;
		char c;
	}
	writefln("%s %s %s",A.init.a.offsetof     //0
					   ,A.init.b.offsetof     //4
					   ,A.init.c.offsetof);   //8
	
	//Align , only to be used with non-pointers
	//Pointers are aligned at size_t
	struct A2 {
		align(2) char a;
		align(2) int b;
		align(2) char c;
	}
	writefln("%s %s %s",A2.init.a.offsetof   //0
					   ,A2.init.b.offsetof   //2
					   ,A2.init.c.offsetof); //6
	
		struct X{
		char c;
		int i;
		double d;
	}
	
	X x1=X('x',5,5.0);
	X x2=X('x',5);
	X x3=X('x');
	X x4=X();
	
	struct Y {
		static Y opCall(){
			writeln("I am a constructor");
			Y y;
			return y;
		}
	}
	
	Y yy=Y();//I am a constructor
	
	//Assignment operator
	struct W {
		int x;
		ref W opAssign(W a) return {
			this.x=a.x;
			return this;
		}
		ref W opAssign(int x) return {
			this.x=x;
			return this;
		}
		//Assignment operator + , i.e. +=
		ref W opOpAssign(string operator)(int x) return{
			if(operator=="+"){
					this.x+=x;
				}
			return this;

		}
		ref W opUnary(string operator)() return {
			if (operator == "++") {
				++x;
			}
		return this;
		}//opunary
		
		ref W opBinary(string operator)(W w) return {
			if(operator=="+")
				this.x+=w.x;
		return this;
		}
		
		
	}//struct
	W w1;
	W w2;
	w2=w1;
	w2=5;
	w2+=5;
	w2=w1+w2;
	
	//Accessing members
	struct SSS{
		int x;
	}
	SSS sss;
	SSS *psss=&sss;
	(*psss).x=5;
	//Short notation
	psss.x=5;
	
	//Struct constructor
	struct Person {
        int age;
        int height;
        float ageXHeight;
		this(int age, int height) {
		this.age = age;
		this.height = height;
		this.ageXHeight = cast(float)age * height;
		}
	}
	auto person = Person(18, 180);
    //pair : destructor
    //pair : static destructor

}//main
