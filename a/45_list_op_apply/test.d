import std.stdio: write,writeln;

class Node {
	int data;
	Node next;
}

class List {
	Node node=null;
	this(int[] AR){foreach(i ; AR)pushfront(i);} 
	void pushfront(int data) {
		Node newnode=new Node();
		newnode.data=data;
		newnode.next=node;
		node=newnode;
	}//pushfront
	int opApply(int delegate(typeof(Node.data)) dg) {
		Node current = node;
		while (current) {
			if (dg(current.data)) return 1;
			current = current.next;
		}
		return 0;
	}//opApply
}//List

void main(){
	List l=new List([3,2,1]);
	foreach(element; l) writeln(element);
	foreach(element; l) writeln(element);
}//main
