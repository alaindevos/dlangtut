class Animal
{
    abstract int maxSize(); // must be implemented by sub-class
    final float maxSizeInMeters() // can't be overridden by base class
    {
        return maxSize() / 100.0;
    }
}

class Lion: Animal
{
    override int maxSize() { return 350; }
}

void main()
{
    import std.stdio : writeln;
    auto l = new Lion();
    assert(l.maxSizeInMeters() == 3.5);

    writeln(l.maxSizeInMeters()); // 3.5
    
	//classes---------------------------------------------------------------
	//->constructors, destructor 
	//-> methods, inheritance, encapsulation, interfaces, abstract class, nested 
    // ---- CLASSES ----
    // Classes model real world objects / systems
    // by storing their attributes and
    // simulating their capabilities
    // Classes limit access to data (Encapsulation)
    // Allow fields and methods to be Inherited
    // Allow subclasses to be treated as super
    // classes while maintaining changes in the subclasses
    // Polymorphism
    class Animal{
      string name;
      double height;
      double weight;
      string sound;
       // static fields belong to the class
      // and their values are shared by all
      // objects
      static int numOfAnimals;
      // Function called when Animal objects
      // are created
      this(string name, double height,
        double weight, string sound){
          // this allows us to refer to the
          // object even though we don't know
          // its name
          this.name = name;
          this.height = height;
          this.weight = weight;
          this.sound = sound;
          numOfAnimals++;
        }
         // If this was final void makeSound
        // I couldn't override it
        void makeSound(){
          writeln(this.name, " says ",
          this.sound);
        }
        void getInfo(){
          writefln("%s is %.2f inches %.2f lbs and says %s",
          this.name, this.height, this.weight, this.sound);
        }
        // static methods are normally utility functions
        // that it wouldn't make sense for the object
        // to be able to perform but you need it
        static void getNumOfAnimals(){
          writeln("Num of Animals : ",
          numOfAnimals);
        }
    }
     // I can inherite everything is Animal
    // and make changes as I wish
    // You can only inherit 1 class
    class Dog : Animal{
      string owner;
      // Have the Animal constructor initialize
      // all fields in both
      this(string name, double height,
        double weight, string sound,
        string owner){
          super(name, height,
            weight, sound);
          this.owner = owner;
      }
      // Override get info
      override void getInfo(){
        super.getInfo();
        writefln("%s's owner is %s",
        this.name, this.owner);
      }
 
    }
     Animal jake = new Animal("Jake", 35,
    140, "Woof");
    jake.getInfo();
    Dog paul = new Dog("Paul", 20, 85,
    "Grrr", "Robert Hansen");
    paul.getInfo();
    // As long as the same methods exist
    // in the super class and the subclass
    // you can refer to the subclass as
    // if it is its super class type
    // Polymorphism
    Animal john = new Dog("John", 20, 85,
    "Grrr", "Larry Eyler");
    john.getInfo();
    john.getNumOfAnimals();

}

