import std;
void main()
{
    //LIST
    // arr = [i for i in range(10)]
    writeln(iota(10));
    // [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    
    //DICTONARY
    // y = {i:v for i,v in enumerate(x)}
    auto x = [2,45,21,45];
    auto y = enumerate(x).assocArray;
    writeln(y);
    // [0:2, 3:45, 2:21, 1:45]

    //CONDITIONAL
    // arr = [i for i in range(10) if i % 2 == 0]
    auto arr = iota(10).filter!(i => i % 2 == 0);
    writeln(arr);
    // [0, 2, 4, 6, 8]

    // arr = ["Even" if i % 2 == 0 else "Odd" for i in range(10)]
    auto arr2 = iota(10).map!(x => x % 2 ? "Odd" : "Even");
    writeln(arr2);
    //["Even", "Odd", "Even", "Odd", "Even", "Odd", "Even", "Odd", "Even", "Odd"]

    //NESTED FOR LOOP
    // arr = [[i for i in range(5)] for j in range(5)]
    auto arr3 = iota(5).map!(j => iota(5));
    writeln(arr3);
    // [[0, 1, 2, 3, 4], [0, 1, 2, 3, 4], [0, 1, 2, 3, 4], [0, 1, 2, 3, 4], [0, 1, 2, 3, 4]]

    // arr = [(i,j) for j in range(2) for i in range(2)]
    auto arr4 = iota(2).cartesianProduct(iota(2));
    writeln(arr4);
    //[Tuple!(int, int)(0, 0), Tuple!(int, int)(0, 1), Tuple!(int, int)(1, 0), Tuple!(int, int)(1, 1)]

    //FLATTEN 2D ARRAY
    // arr = [i for j in x for i in j]
    auto x2 = [[0, 1, 2, 3, 4],[5, 6, 7, 8, 9]];
    auto arr5 = x2.joiner;
    writeln(arr5);
    // 0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

} 
