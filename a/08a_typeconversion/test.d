import std: array,joiner,map,format;
import std.algorithm: filter,sort;
import std.algorithm.iteration: sum;
import std.conv: to;
import std.exception: assumeUnique;
import std.stdio: writeln,writefln;
import std.string: representation;

void main(string[] args){

//Implicit conversion for user-defined types
//D officially forbid implicit conversions for user-defined types to avoid the pitfalls associated with them.
//But defining an implicit conversion is actually possible by abusing alias this.

struct NumberAsString
{
    private string value;
    this(string value)
    {
        this.value = value;
    }

    int convHelper()
    {
        return to!int(value);
    }
    alias convHelper this;
}//struct

auto af = NumberAsString("123");
int bf = af; // implicit conversion happening here
writefln("%d", bf);


//type conversion-------------------------------------------------------
// ---- CASTING ----
// You can convert from one type to another
// Convert an int into a short
int b,x2;
int cInt = 15;
short cShort = cast(short) cInt;
// Casting causes issues if container is small
short cShort2 = cast(short) 32768;
writeln("Short Cast : ", cShort2);//-32768
// Convert int to string and show type
auto sInt = to!string(10);
writeln("Type : ", typeof(sInt).stringof);//string
// Convert string to int and show type
auto iStr = to!int("10");
writeln("Type : ", typeof(iStr).stringof);//int

//Implicit
char xa=5;
int xb=xa;
double xc=1;
float xd=xc;

//Narrowing
auto f=100 ; // int
ubyte g1=cast(ubyte)f;

//Signed to unsigned or vice-versa is ok
ubyte k=255;
byte  l1=k;   //-1

//To
int aa=42;
float bb=4.2;
bb=to!float(aa);
bb=aa.to!float;
aa=('9'-'0').to!int;

writeln(to!string(10)); //10

char [] c1 = "Hello".dup;
ubyte [] u=c1.representation;

int [] a=[1,0,1,1,1,0,1,0,1,1,1,0];

string s = format!"%-(%s%)"(a);
writeln(s);                         //101110101110
writeln("-");

dchar[12] b1 = a.map!(to!string).joiner.array;
writeln(b1);                         //101110101110
writeln("+");

ubyte[] conv = a.to!(ubyte[]);
conv[]+='0';
writeln(cast(string)conv);          //101110101110
writeln("*");

auto r1 = a.map!(i => cast(char)(i + '0'));
writeln(r1);                         //101110101110
writeln("/");

auto i1=to!int("123");
auto i2="123".to!int();
auto i3="123".to!int;   //Treat function like property.

auto x11=[1,2,3].map!(to!long);
writeln(typeid(x11));    // std.algorithm.iteration.MapResult!(to, int[]).MapResult
auto x22=x11.filter!(x => x%2);
writeln(typeid(x22));    //test.main.FilterResult!(__lambda2, MapResult!(to, int[])).FilterResult
long x4=sum(x22);
writeln(typeid(x4));    //long

auto r2=to!(double[])([1,2,3]);
auto r3=to!(immutable(double[]))([1,2,3]);

int [] x5=[3];
auto x6=x5.assumeUnique; //Convert to immutable;
writeln(typeid(x6));    //immutable(int)[]

struct showMe{
	string toString(){
		return "I am showMe";
		}
	}
showMe sss=showMe();
writeln(sss);		      //Prints I am showme

//string to ubyte
string ss1 = "unogatto";
immutable(ubyte[]) ustr1 = cast(immutable(ubyte)[])ss1;
string ss4 = "unogatto";
ubyte[] mustr2 = cast(ubyte[])ss4;

//ubyte to string
ubyte[] stream = [ 0x75, 0x6e, 0x6f, 0x67];
string us  = cast(string)stream;

struct Dumper{
	//in: The parameter is an input to the function.
	void put(in void[] data){
		auto data2=cast(const ubyte []) data;
		foreach(b; data2)writeln("%02x ", b);// 84,101,115,116
		}//put
	}//Dumper

Dumper myoutput;
string s8="Test";
myoutput.put(s8);

int [] x9=[ 1,3,8,2,0];
	x9.sort!(
		(a,b)
			{
			   bool ret= a > b;
			   return ret;
			}
	);//Sort
foreach (int t;x9)writeln(t);//8,3,2,1,0

}
