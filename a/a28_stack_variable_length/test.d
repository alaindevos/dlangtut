import std.stdio: writeln;

struct IRange {
    int [] _elements;
    bool empty() { return _elements.length == 0; }
    int front() { return _elements[$-1]; }
    void popFront() { _elements.length -= 1; }

}

struct IStack {

    private int [] _array;

    void push(int element) {
        _array ~= element;
    }

    void pop() {
        assert(!isEmpty);
        _array.length -= 1;
    }
    ref int top() {
        assert(!isEmpty);
        return _array[$-1];
    }
    bool isEmpty() { return _array.length == 0; }

    auto elements() { return IRange(_array); }
}

void main(){
    IStack stack;
    foreach(i; 0..5) stack.push(i);
    writeln("Iterating...");
    foreach(i; stack.elements) writeln(i);
    stack.pop();
    stack.pop();
    writeln("Iterating...");
    foreach(i; stack.elements) writeln(i);
}

