For Example
import core.stdc.stdlib;
auto p = cast(T*)malloc(length * T.sizeof);
assert(!length || p);
auto array = p[0 .. length];
scope (exit) free(array.ptr)
