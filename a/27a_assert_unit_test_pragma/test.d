//Use assert to catch logic errors not to verify user input !!!!!
//Use exception to validate user input
//Unit Test work together with the Ddoc documentation tool
//Compile "-D" to create html


module test;

@safe

import std.stdio: writeln;
import std.conv:text;
import std.exception:enforce;


//Compile time message
pragma(msg,"Test compile time message");


//Compile a function inline
pragma(inline,true){
	void testme(){writeln("Hallo");}
}

//Known versions
version(Windows)
	pragma(msg, "We are compiling on Windows.");
else version(OSX)
	pragma(msg, "We are compiling on a Mac OS X system.");
else version(Posix)
	// THIS ONE IS PRINTED !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	pragma(msg, "We are compiling on a Posix system.");


/*Compile-time assert , also in release version !!! */
static assert( 1 < 2 );


/** test if halved has embedded documentation */
int halved(int value){
	return value/2;
}//halved
//Place a unittest imidately after the function it is testing.
@safe @nogc pure nothrow unittest{
	assert(42.halved == 21);
}//unittest


//To enable unittests in an executable, pass the -unittest flag to the compiler.
//This will cause DRuntime to run each unit test when the program is executed after
//static constructors and before main.

/++ Embeded code test
+/
unittest{
	assert(40.halved == 20);
}//unittest


//Executed during compilation
unittest {
	int x = 1;
	x++;
	assert(x==2);
}

//Pre-condition, post-condition
double fun(double x)
in { //Pre-condition,
	assert(x>0);
}//in
out { //Post condition
	assert(x>0);
}
// Body is implicit when not mentioned !!!
body{
//Implementation
return 0;
}

//-------------------------------------------------------------------
class CustomException : Exception {
	private string origin;
	private double val;
	this(string msg,string origin,double val){
		super(msg);
		this.origin=origin;
		this.val=val;
	}//constructor
	override string toString(){
		return text(origin,":",super.toString(),val);
	}//tostring
}//class	

//Pre-condition, you best don't use an exception !!!!!!!!!!
double fun2(double x)
in {
	if (!(x >=0)){
		throw new CustomException("myexception","fun",x);
	}
}//in
body{
//Implementation
return 0;
}

//Invariant, a continu check in a class........................
//Non-private and non-static member FUNCTIONS cannot be called from inside an
//invariant. Attempting to do so will enter an infinite loop,
class Number {
	private: 
		uint x;
	invariant(){
		assert(x>0);
	}//invariant
}//class

struct NumberS {
	private: 
		uint x;
	invariant(){
		assert(x>0);
	}//invariant
}//class


void main(){
	
	
	//Function contracts
	void printNotGreaterThan42(uint number)
        //Contract programming in block , preconditions
		in {
    			assert(number < 42);
		}
		body {
    			import std.stdio : writeln;
    			writeln(number);
		}

	//Class Contracts
	class OlderThanEighteen {
    		uint age;
		final void driveCar()
    		in {
         		assert(age >= 18); // variable must be in range
    		}
    		body {
         		// step on the gas
    		}
	}
	
	//alias assigns alias to existing names
	alias myint=int;
	myint iii=5;
	
	Number n=new Number();
	n.x=5;
	assert(n) ; // Checks invariant
	
	NumberS s;
	s.x=5;
	assert(&s) ; //Checks invariant
	
	writeln("Start");
	int a,b;
	// throw assert exception at runtime 
	assert(a==b, "a and b are different");
	assert(a==b, text(a," and ",b," are different"));
	
	// enforce , is also compiled in release mode compilation !!!!!!!
	enforce(a==b,"a and b are different");
	int count=3;
	enforce(count>3,"Count must be >3");

	
	//assert-false-----------------------------------------------------
	//assert(false);           // On a point your code should not come

	writeln("Done");
	debug {writeln("DEBUGLINE");
		}
	else{
		 writeln("NODEBUG");
		}
		
		


}//main
