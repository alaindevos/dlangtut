import std.stdio:writefln,writeln;
int main(){
alias f1 = int delegate(int);
alias f2=delegate int(int number) { return number; };
int delegate(int) f3=(int x)=>x+1;
f1 x = f2;
int f4(int x)
    {return x*2;}
f1 f5 = & f4;
writeln(typeid(typeof(f3)));
writeln(typeid(typeof(f4)));
writeln(typeid(typeof(f5)));
return 0;
}
