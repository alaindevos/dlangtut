import std.exception : enforce ;

void main(){

class List {
	private int payload;
	private List next;
	this(int [] data) {
		enforce(data.length);
		payload=data[0];
		if(data.length==1) return;
		next=new List (data[1..$]);
	}//this
}//List

int [3] a=[1,2,3];
List l=new List(a);

}//main
