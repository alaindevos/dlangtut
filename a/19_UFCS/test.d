import std.stdio;

bool isPrime(int number) {
    foreach(i; 2..number) {
        if (number % i == 0) {
            return false;
        }
    }

    return true;
}

void main() {
    writeln(2.isPrime);
    writeln(3.isPrime);
    writeln(4.isPrime);
    5.isPrime.writeln;
}
