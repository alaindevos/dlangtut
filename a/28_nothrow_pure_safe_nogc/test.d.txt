[01;34mmodule[m test[31m;[m

[01;34mimport[m std[31m.[mstdio[31m:[mwriteln[31m;[m

[01;34mversion[m [31m=[m Today[31m;[m

[32mint[m [01;30maddone[m[31m([m[32mint[m a[31m)[m [31m{[m[01;34mreturn[m a[31m+[m[35m1[m[31m;[m[31m}[m

[32mint[m [01;30maddtwo[m[31m([m[32mint[m a[31m)[m [31m{[m[01;34mreturn[m a[31m+[m[35m2[m[31m;[m[31m}[m


[01;34malias[m funp[31m=[m[32mint[m [01;34mfunction[m[31m([m[32mint[m[31m);[m

[32mint[m [01;30mmyapply[m[31m([m[37mfunp[m fun[31m,[m[32mint[m value[31m)[m
[31m{[m
	[01;34mreturn[m [31m(*[mfun[31m)([mvalue[31m);[m
[31m}[m


[37mfunp[m [01;30mmyapply2[m[31m([m[32mint[m value[31m)[m
[31m{[m
[01;34mreturn[m value[31m==[m[35m1[m [31m?[m [31m&[maddone [31m:[m [31m&[maddtwo[31m;[m
[31m}[m

[01;34mvoid[m [01;30mmain[m[31m()[m[31m{[m

[31m// No garbage collection used	[m
[32mdouble[m [01;30msqrt1[m[31m([m[32mdouble[m x[31m)[m @nogc [31m{[m
	[01;34mreturn[m x[31m*[mx[31m;[m
[31m}[m[31m//sqrt[m

[31m// Pure means no side effects	[m
[32mdouble[m [01;30msqrt4[m[31m([m[32mdouble[m x[31m)[m [01;34mpure[m [31m{[m
	[01;34mreturn[m x[31m*[mx[31m;[m
[31m}[m[31m//sqrt[m

[31m// nothrow, no exceptions generated[m
[32mdouble[m [01;30msqrt2[m[31m([m[32mdouble[m x[31m)[m [01;34mnothrow[m [31m{[m
	[01;34mreturn[m x[31m*[mx[31m;[m
[31m}[m[31m//sqrt[m

[31m//No will not do ugly casts[m
[32mdouble[m [01;30mdoit[m[31m()[m @safe [31m{[m
	[01;34mreturn[m [35m0[m[31m;[m
[31m}[m

[31m//Low level access[m
[32mdouble[m [01;30mdoit2[m[31m()[m @system [31m{[m
	[01;34mreturn[m [35m0[m[31m;[m
[31m}[m

[01;34mdeprecated[m [32mdouble[m [01;30mdoit3[m[31m()[m  [31m{[m
	[01;34mreturn[m [35m0[m[31m;[m
[31m}[m

[01;34mversion[m[31m([mToday[31m)[m[31m{[m
	[32mdouble[m [01;30mdoit4[m[31m()[m @system [31m{[m
		[01;34mreturn[m [35m0[m[31m;[m
	[31m}[m
[31m}[m[31m//version[m

[32mint[m a[31m=[m[01;30mmyapply[m[31m(&[maddone[31m,[m[35m1[m[31m);[m
[01;30mwriteln[m[31m([ma[31m);[m
[37mfunp[m p[31m=[m[01;30mmyapply2[m[31m([m[35m2[m[31m);[m
[01;30mwriteln[m[31m((*[mp[31m)([m[35m2[m[31m));[m

[31m}[m[31m//main[m
