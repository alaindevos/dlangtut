module test;

import std.stdio:writeln;

version = Today;

int addone(int a) {return a+1;}

int addtwo(int a) {return a+2;}


alias funp=int function(int);

int myapply(funp fun,int value)
{
	return (*fun)(value);
}


funp myapply2(int value)
{
return value==1 ? &addone : &addtwo;
}

void main(){

// No garbage collection used	
double sqrt1(double x) @nogc {
	return x*x;
}//sqrt

// Pure means no side effects	
double sqrt4(double x) pure {
	return x*x;
}//sqrt

// nothrow, no exceptions generated
double sqrt2(double x) nothrow {
	return x*x;
}//sqrt

//No will not do ugly casts
double doit() @safe {
	return 0;
}

//Low level access
double doit2() @system {
	return 0;
}

deprecated double doit3()  {
	return 0;
}

version(Today){
	double doit4() @system {
		return 0;
	}
}//version

int a=myapply(&addone,1);
writeln(a);//2
funp p=myapply2(2);
writeln((*p)(2));//4

}//main
