import std.stdio;

void main(){
	struct MyStack {
		float [] buf=null;
		bool isEmpty(){return buf==null;}
		void push(float item){
			buf=buf~item;
		}
		float pop(){
			float f=buf[buf.length-1];
			buf=buf[0..buf.length-1]; // Or buf.length--;
			return(f);
		}
	}
	MyStack s;
	s.push(1);
	s.push(2);
	s.push(3);
	writeln(s.pop());
	writeln(s.pop());
	writeln(s.pop());
}
