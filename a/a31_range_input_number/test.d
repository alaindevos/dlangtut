import std.stdio;

struct NumberRange {
	int begin;
	int end;
//Called each time
	invariant() { assert(begin<=end);	}
//A const function does not change its "owner"	
	bool empty() const {return begin==end;}
	void popFront() {++begin;}
//A const function does not change its "owner"	
	int front() const {return begin;}
}

void main() {
    foreach(element; NumberRange(3,7)) { 
		write(element,' ');
		} 
    for ( auto myObject=NumberRange(3,7); !myObject.empty(); myObject.popFront()) {
		auto element = myObject.front();
		write(element,' ');
	}
}
