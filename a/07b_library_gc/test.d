import core.memory: GC;
import core.stdc.math: hypot;
import std.algorithm: find,sort;
import std.algorithm.iteration: filter;
import std.math: abs,ceil,floor,exp,round,log,log10,pow,atan,sinh,cosh,sqrt,tan,asin,acos,PI,sin,cos,tanh,cbrt;
import std.getopt: getopt;
import std.process: executeShell,environment;
import std.regex: regex,matchAll;
import std.stdio: writeln;
import std.range.primitives: walkLength;
import std.random: uniform,Random;
import std.datetime.systime: Clock;
import std.datetime.timezone: UTC;


//GC--------------------------------------------------------
void myfun(){
class C{
	int[10000] x;
	}//class C

struct S {
	C c=null;
	@disable this();

	this(int dummy) {
		c=new C();
		writeln("Constructor");
		};//Constructor

	~this(){
		writeln("Destructor");
		.destroy(*((c.x).ptr));
		.destroy(c);
		void * address=GC.addrOf(cast(void *)c);
		GC.free(address);
		};//destructor
}//struct S
S mys=S(0);
};//myfun()


void main(string[] args){

// ---- MATH ---------------------------------------------------------
writeln("5 + 4 = ", (5+4));
writeln("5 - 4 = ", (5-4));
writeln("5 * 4 = ", (5*4));
writeln("5 / 4 = ", (5/4));
writeln("5 % 4 = ", (5%4));
// Math done on integers default to
// integer output and doubles return doubles
writeln("5 / 4 = ",(5.0/4.0));
// incMe++ same as incMe = incMe + 1
// Can also decrement with --
int incMe = 0;
writeln("incMe: ",(incMe++));//0
writeln("incMe: ",(++incMe));//2
// Another shortcut
incMe += 10;
// Numerous math functions
writeln("abs(-1) = ", abs(-1));
writeln("ceil(4.25) = ", ceil(4.25));
writeln("floor(4.25) = ", floor(4.25));
writeln("round(4.25) = ", round(4.25));
writeln("exp(1.0) = ", exp(1.0));
writeln("log(1) = ", log(1));
writeln("log10(1) = ", log10(1));
writeln("pow(2,2) = ", pow(2,2));
writeln("sqrt(4) = ", sqrt(4.0));
writeln("cbrt(4) = ", cbrt(4.0));
writeln("hypot(5,5) = ", hypot(5,5));
writeln("PI = ", PI);
// Trig Functions Radians
writeln("sin(1.5708) = ", sin(1.5708));
writeln("cos(1.5708) = ", cos(1.5708));
writeln("tan(1.5708) = ", tan(1.5708));
writeln("asin(1.5708) = ", asin(1.5708));
writeln("acos(1.5708) = ", acos(1.5708));
writeln("atan(1.5708) = ", atan(1.5708));
writeln("sinh(1.5708) = ", sinh(1.5708));
writeln("cosh(1.5708) = ", cosh(1.5708));
writeln("tanh(1.5708) = ", tanh(1.5708));


// ---- DATE ----
auto nowTime = Clock.currTime(UTC());
writeln("Year",nowTime.year);//2022
writeln("Month",nowTime.month);//apr
writeln("Day",nowTime.day);//23
writeln("Hour",nowTime.hour);//2
writeln("Minute",nowTime.minute);//11
writeln("Second",nowTime.second);//34

// ---- RANDOM NUMBERS ----
// Generate random number from 5 to 20
// Generate random seed
int seed = nowTime.second;
auto rand = Random(seed);
writeln("Rand : ", uniform(5, 20, rand));//8

//ARGS-----------------------------------------------------------------
	int myarg1,myarg2;
getopt(args,
	"myarg1",&myarg1,
	"myarg2",&myarg2);
writeln("myarg1:",myarg1);//0
writeln("myarg2:",myarg2);//0
writeln("PATH:",environment.get("PATH"));

//REGULAR EXPRESSION--------------------------------------------------------
auto r = regex(`([a-z])a`);
auto result = "banana".matchAll(r);
writeln(result); // [["ba", "b"], ["na", "n"], ["na", "n"]]
writeln(result.walkLength); // 3

//EXECUTE-SHELL----------------------------------------------
auto cmd = executeShell("/bin/ls");
if (cmd.status != 0) writeln("ls failed", cmd.output);
string resultx=cmd.output;
writeln(resultx);

//GARBAGE COLLECTION--------------------------------------------------------
myfun();
GC.collect(); //Start a garbage collector cycle
int size=int.sizeof;
int number=100;
void * buffer = GC.calloc(number*size);
int * intBuffer = cast(int*)buffer;
.destroy(*intBuffer);    // !!! The pointéé
GC.free(intBuffer);

}
