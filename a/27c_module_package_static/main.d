//A D-source file forms a module
//D-directory of D-source files forms a package
//The name of the directory is the name of the package
//The compiler finds the module files by converting the package and module names
//directly to directory and file names.


//imports are private by default
import std.stdio:writeln;
import mylibrary : sayhi;
import alib = mylib.mylibrary2;

//import but only with explicit symbol qualification
//static import mylibrary3; 
//public import, whoever import this also import the public import
//public import mylib.mylibrary2

//Module constructor, run before main
static this(){
	writeln("module constructor");
}

//Module destuctor, run after main
static ~this(){
	writeln("module destructor");
}

// Static has no effect in module scope !!!!!!!!!!!!
static int a1;

//Only accessible in this package and subpackages
//package(mypackage.subpackage) int a ; //To allow superpackages
package int a2;

void foo(){
	// Static variable persist in lifetime beyond scope of function
	// Static variables are thread-local , each thread get its own copy
	static int a;

	// Do not initalise variable
	int b= void ;
}

void main(){
    //module constructor
	sayhi(); //Say Hi
	alib.sayhi2(); //sayhi2
	foo(); //foo declared after ...
    //module destructor
}
