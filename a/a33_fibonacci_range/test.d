import std.stdio: writeln;
import std.range: take,Take,recurrence;

void main(){

auto fib1 = recurrence!((a,n) =>a[n-1] + a[n-2])(1,1);
fib1.take(42).writeln();

auto fib2 = recurrence!("a[n-1] + a[n-2]")(1, 1);
fib2.take(42).writeln();


struct fib3
{
    int current = 0;
    int next = 1;

    enum empty = false;   // ← infinite range

    int front() const {
        return current;
    }

    void popFront() {
        const nextNext = current + next;
        current = next;
        next = nextNext;
    }
}

fib3 f;
f.take(43).writeln();
}
