import std.stdio;
void main(){
    struct List {
        struct Node {
            float f;
            Node *next=null;
            }
        Node * root=null;
        bool empty() const {return !root;}
        void popFront() {root=root.next;}
        float front() const {return root.f;}
        void pushfront(float f) {
				Node * newnode=new Node();
                newnode.f=f;
				newnode.next=root;
				root=newnode;
               }
		void pushend(float f){
                Node * newnode=new Node();
                newnode.f=f;
                Node *t=root;
                if(t==null)
					{t=newnode;}
				else{
					while(t!=null && t.next!=null)
						{t=t.next;}
					t.next=newnode;
					}
			}
		void printall(){
			Node *l=root;
			for( ; l ; l=l.next){
				writeln(l.f);
			}
		}
    }
    List * l=new List();
    l.pushfront(2);
    l.pushfront(1);
    l.pushend(3);
    l.pushend(4);
    foreach(element; *l) writeln(element);
    (*l).printall();
}
