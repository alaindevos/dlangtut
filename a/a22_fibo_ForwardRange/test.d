/// Requires:empty,front,popfront,save
import std.stdio: write,writeln;
import std.range: empty,popFront,front,take;
import std.typecons: Tuple;

void myprint(T)(T range) {
    for ( ; !range.empty; range.popFront()) write(' ', range.front);
    writeln();
}


struct FibonacciSeries{

    int current = 0; //state
    int next = 1;

    bool empty(){ return false;} //infinite

    int front() const { return current; }

    void popFront() {
        const nextNext = current + next;
        current = next;
        next = nextNext;
    }
    
    FibonacciSeries save() const { return this;} //Forwardrange
}


void main(){
    auto f=FibonacciSeries().take(10);
    auto g=f.save();
    myprint(g);
}
