import std.stdio:writefln;

int [] GV=[1,2];
int [2] GF=[1,2];

void main(){

//Build in type on heap
int* p = new int;  // Works

writefln("-------------");
writefln("GV:address :%12x:AAA",&GV);
writefln("GF:address :%12x:AAA",&GF);
writefln("GV:ptr     :%12x:BBB",GV.ptr);
writefln("GF:ptr     :%12x:AAA",GF.ptr);

{
static int [] SLV=[1,2];
static int [2] SLF=[1,2];

writefln("------------");
writefln("STATIC LV:address :%12x:stack",&SLV);
writefln("STATIC LF:address :%12x:stack",&SLF);
writefln("STATIC LV:ptr     :%12x:heap",SLV.ptr);
writefln("STATIC LF:ptr     :%12x:stack",SLF.ptr);
}
{
int [] LV=[1,2];
int [2] LF=[1,2];

writefln("------------");
writefln(".LV:address :%12x:stack",&LV);
writefln(".LF:address :%12x:stack",&LF);
writefln(".LV:ptr     :%12x:heap",LV.ptr);
writefln(".LF:ptr     :%12x:stack",LF.ptr);
}

}

//GV:address :   8002d6120:stack
//GF:address :   8002d6130:stack
//GV:ptr     :      2986c8:heap
//GF:ptr     :   8002d6130:stack
------------
//STATIC LV:address :   8002d6138:stack
//STATIC LF:address :   8002d6148:stack
//STATIC LV:ptr     :      2986d0:heap
//STATIC LF:ptr     :   8002d6148:stack
------------
//.LV:address :7fffffffe010:stack
//.LF:address :7fffffffe008:stack
//.LV:ptr     :   801000000:heap
//.LF:ptr     :7fffffffe008:stack
