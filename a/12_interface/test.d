import std.stdio;

//Interface : Only member functions without implementation
//Member functions are abstract even without using the abstract keyword
//Member functions that implement it must be static or final
//Member variables must be static
//Interfaces can inherit only interfaces
interface Stat {
	void accumulate(double x);
	double result();
	final void doit();

	static int s;
	//Static functions must be implemented
	static void doit2(){writeln("Hallo");s=5;}
}

interface Stat2 {
	double mystat2();
}

//Implemenatation must implement functions, or call functions abstract
class Min : Stat,Stat2 {
	private double min= double.max;
	
	final void accumulate(double x){
		if(x<min)
			min=x;
		}
	
	double result(){
		return min;
		}
		
	double mystat2(){
		return 0;
		}
}	

int main(){
	Min M=new Min;
	Stat S=M; // OK !!!!, can also be passed to functions as parameter.
	S.accumulate(1);
	S.accumulate(2);
	writeln(S.result); //1
	// S.doit(); : Link error
	
	//Call destructor explicit
	destroy(S);

	
//Interface-----------------------------------------------------------
    // If you want to add functionality to
    // a class and it doesn't make sense for
    // that functionality to be made into an
    // object you may need an interface
    interface Drivable{
      // You define, but don't implement methods
      // in an interface
      void move();
      void stop();
    }
     // You can implement multiple interfaces by
    // just separating them with a comma
    class Vehicle : Drivable{
      void move(){}
      void stop(){}
    }

//Abstract class ------------------------------------------------------
    // Abstract classes define functions that
    // must be implemented, but they can also
    // define implementation unlike interfaces
    class Flyable{
      abstract void fly(){
        writeln("I'm flying");
      }
       abstract void crash();
    }
 
	return 0;
}
