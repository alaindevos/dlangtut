import std.stdio;
import std.range;
void main(){
	struct Fibo {
		int previous=0;
		int current=0;
		int next=1;
		
		//inputrange defines iterator
		//@property bool empty();
    		//@property T front();
    		//void popFront();
    		void popFront(); 
		bool empty() const {return false;}
		void popFront() {
			previous=current;
			current=next;
			next=current+previous;
			}
        	float front() const {return current;}
	}
	auto numbers=Fibo();
	writeln(numbers.take(15));
}
