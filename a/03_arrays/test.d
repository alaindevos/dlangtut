import std.algorithm: remove,reverse;
import std.algorithm.sorting: sort;
import std.array: Appender,replace,split;
import std.complex: complex;
import std.conv: octal,to;
import std.datetime: Date,TimeOfDay;
import std.datetime.date: DateTime;
import std.stdio: writeln,writefln;
import std.typecons:tuple,Tuple,No;
import std.uni: isLower,isUpper,isAlpha,isWhite,toLower,toUpper;
import std.variant: Variant;
import std.string: indexOf,lastIndexOf,isNumeric;


int [4] sar=new int[4];         ////STATIC ARRAY,Static in data-segment,i.e. not on stack

void main(){

// ---- ARRAYS ----Single dimension-----------------------------------
	// Arrays store multiple values in boxes
	// in memory
	// Fixed size array
	int[10] a1;
	alias T = int[4];

	// This line does not compile Error: new can only create structs, dynamic arrays or class objects, not int[4]'s
    // Error: cannot create a int[4] with new
    // auto tk = new T; 
	
    auto tj= [1,2,3,4];

	// Set values
	int[] a2 = [1,2,3,4];
    //Make mutable copy
    auto a7=a2.dup;
    //Make immutable copy
    auto a6=a2.idup;

    // Length
    ulong la2=a2.length;
	// Change using index
	a2[0] = 0;
	// Get value with index
	writeln("Index : ", a2[0]);//0
	// Create a resizable array (Dynamic)
	int[] a3;
	// Add values
	a3 ~= 1;
	a3 ~= 2;
	a3 ~= 3;
	// Remove element at index
	a3 = a3.remove(1);
	writeln(a3);//[1,3]
	// Combine arrays
	a3 = a2 ~ a3;
	writeln(a3);//[0,2,3,4,1,3]
	// Remove odd values
	a3 = a3.remove!(x => (x%2) == 0);
	writeln(a3);//[3,1,3]
	// Sort and reverse
	writeln(sort(a3));//[1,3,3]
	writeln(reverse(a3));//[3,3,1]
	// Replace all matches
	writeln(a3.replace(3,2));//[2,2,1]
	// Get a range of values
	a3 = a2 ~ a3;
	writeln(a3[0 .. 3]);//[0,2,3]
	// Duplicate array
	auto a4 = a3.dup;
	// Perform operation on every value
	a4[] *= 2;
	writeln(a4);//[0, 4, 6, 8, 6, 6, 2]

    //Conceptual array
	struct intarray{
		int* ptr;
		size_t length;
	}
	int[] a=[1,2,3];
	size_t l1=a.length;
	int * p=a.ptr;

	//STATIC ARRAY i.e. not on stack
	static int []  s21=new int[4]; //Static in data-segment,i.e. not on stack
	
	//FIXED_SIZED ARRAY on stack
	int [3] [4] eee;
	writeln(eee.sizeof);        // 48 !!!
	writeln(eee[0].sizeof);     // 12 !!!
	writeln(eee[3][2].sizeof);  // 4 !!!
	writeln(eee.length);        // 4!!!
	writeln(eee[0].length);     // 3!!!

//COPY::::

    //A) F->F : COPY
    //B) V->V : ASSIGN
    //C) V->(dup)V : COPY
    //D) F->V : ASSIGN
    //E) V->F  : COPY

	//A).COPY FIXED SIZE TO FIXED SIZE, copys all elements, i.e. deep copy 
	int[2] source=[1,2];
	int[2] destination=[3,4];
	destination=source;
	writeln("Fixtest1:",destination[0]); //1  !!!!!!!!!!!!!!!!!!!!!!!
	source[0]=1234;
	writeln("Fixtest2:",destination[0]); //1 , ie other arrays
	

	//-----------------------------------------------------------------
	//------------------------------------------------------------------
	//------------------------------------------------------------------
	//VARIABLE_LENGTH ARRAY on HEAP - A pair of a pointer and a length--
	int[] cc;
	//cc.ptr == null ;
	//cc.length == 0;
	int[] aa=[0,1,2];
	writeln(aa[0..$]); // $=a.length=3 ; [0,1,2];
	writeln(aa.length); //3 , Length
//Reserve capacity
    aa.reserve(20);

	//B).ASSIGN VARIABLE LENGTH TO VARIABLE LENGTH !!!!!!!!!!!!!!!!!!!!!!!
	int[] g=[1,2];
	int[] h=[3,4];
	h=g;
	writeln("Vartest1:",h[0]); //1  !!!!!!!!!!!!!!!!!!!!!!!
	g[0]=1234;
	writeln("Vartest2:",h[0]); //1234 , i.e. same array

	//C).COPY VARIABLE LENGTH TO VARIABLE LENGTH, using DUP !!!!!!!!
	int[] qa=[1,2];
	int[] ra=[3,4];
	ra=qa.dup;
	writeln("Varduptest1:",ra[0]); //1  !!!!!!!!!!!!!!!!!!!!!!!
	qa[0]=1234;
	writeln("Varduptest2:",ra[0]); //1 , ie different array !!!!

	//****************************************************************

	//D).ASSIGN FIXED LENGTH TO VARIABLE LENGTH !!!!!!!!!!!!!!!!!!!!!!!
	int[2] f1=[1,2];
	int[]  v1=[3,4];
	v1=f1; //Let variable length array point to fixed length array
	writeln("Assigntesta:",v1[0]); //1  !!!!!!!!!!!!!!!!!!!!!!!
	f1[0]=1234;
	writeln("Assigntestb:",v1[0]); //1234 , ie same array , v behaves like a pointer
	writeln("Address1:",&f1);      //7FFFFFFFDE88
	writeln("Address2:",&v1);      //7FFFFFFFDE78
	writeln("Address3:",f1.ptr);   //7FFFFFFFDE88
	writeln("Address4:",v1.ptr);   //7FFFFFFFDE88, Points to address of f1 on the stack !!!!!!!!!!!!!!!!!!!!!!!!

	//E).COPY VARIABLE LENGTH TO FIX LENGTH !!!!!!!!!!!!!!!!!!!!!!!
	int[]  v2=[1,2];
	int[2] f2=[3,4];
	f2=v2; // Copies elements from heap to stack
	writeln("Assigntestc:",f2[0]); //1  !!!!!!!!!!!!!!!!!!!!!!!
	v2[0]=1234;
	writeln("Assigntestd:",f2[0]); //1  , ie other array , f behaves like a struct
	writeln("Address5:",&v2);     // 7FFFFFFFDE68,stackaddress
	writeln("Address6:",&f2);     // 7FFFFFFFDE60,stackaddress
	writeln("Address7:",v2.ptr);  // 801001090,Points to address space on HEAP !!!!!!!!!!!!!!!!!!!!!!!!
	writeln("Address8:",f2.ptr);  // 7FFFFFFFDE60,stackaddress

	int[] ab=new int[4];         //Obsolete notation !!!!!!!!!!
	int[] ac=new int[] (4);      //Prefered notation !!!!!!!!!!
	writeln(ac.length);//4


	
	int [] array=new int[4]; // On-stack
	int []    bz=array;
	array.sort();
	array.reverse();
	writeln(array.length); //4
	int t=0;
	
//SLICES (DYNAMIC ARRAY)-----------------------------------------
	int [] slice=array[2..4];             // !!!!!!!!!!!!!!!!!!!!!!!!!!!
	writeln(slice.length);//2
	bz[0]=5;
	writeln(array[0]);//5
	
	//NEXT FUNCTION DO NOT WORK WITH @nogc, you need garbage collector
	array=[1,2,3]~[4,5,6]; //Concatenation !!!!!!!!!!!!!!
	array ~= 8;             //APPEND        !!!!!!!!!!!!!!

	//REFERENCE: do not copy element--------------------------------
	foreach(ref element;array){element=t;t=t+1;}

	//Appender
	static Appender!(int[]) array2;
	array2~=3; //append element
	array2~=[1,2]; //append_array
	
	//Manipulate length
	int[] m;
	m.length=3 ; // Array grows
	writeln(m[$-1]); //Print last element:0 
	
	//remove an element
	m=m.remove(2);

	//Assign "pointers"
	int[] m2=m;
	//Vs copy
	int[] m3=m.dup;
	immutable(int []) m4=m3.idup;
	
	//Universal array
	void[] un;
	
	//Do not call initialiser
	int [3] fff=void ;

	//Equality
	int [] f3=[1,2];
	int [] f4=[1,2];
	writeln( f3 == f4 ) ;         // true
	writeln( f3.ptr == f4.ptr ) ; // False ,different pointer
	
	//Elementwise addition
	double[3] e1=[1,2,3];
	double[3] e2=[4,5,6];
	double[3] e3;
	e3=e1[]+e2[];
	//Fixed length array
	writeln(typeid(e3));     //double[3]
	//Variable length array
	writeln(typeid(e3[]));   //double[]

//MULTI-DIMENTIONAL ARRAY
	enum colx=8; //Columns
	enum rowy=4; //Rows
    enum pagz=2; //Pages
	int[colx][rowy][pagz] z=new int [colx][rowy][pagz]; 
	z[pagz-1][rowy-1][colx-1]=-1;
	writefln("%d",z[pagz-1][rowy-1][colx-1]);  //-1
	
	//2-dim always, first type , then size !!!
	auto ad=new int[][] (4,8);
	int [][] ff1=[[1,2,3],[4,5,6]];
	int [3][] ff2=[[1,2,3],[4,5,6]];
	int [][2] ff3=[[1,2,3],[4,5,6]];
	int [3][2] ff4=[[1,2,3],[4,5,6]];

	//Triangular array
	auto tt=new double[][5];
	foreach(i,ref e;tt){
		e=new double[tt.length-i];
	}

//DUP
	auto an=new int[][] (0,0);
	an~=[1,2];
	an~=[3,4];
	auto bn= an.dup;
	an[0]=[5,6];
	an[1][1]=7;
	writeln(bn);//[[1, 2], [3, 7]]


	// a5[How many down][How many across][How many Groups]
	// a5[3][4][1]


	string[][][] a5 = [[["000"], ["100"], ["200"], ["300"]],
		[["010"], ["110"], ["210"], ["310"]],
		[["020"], ["120"], ["220"], ["320"]]];
	writeln(a5[2][3][0]);//320

	auto myarray=new double [][5] ;
	// Triangular array
	foreach(i, ref e; myarray){
		e = new double [myarray.length - i ];
	}

  enum size_t mycol=64;
  auto matrix = new double [mycol][10];

//POINTER-------------------------------------------
	//pointer ; reference  type; value type
	// ---- POINTERS ----
	// You can work with pointers like C
	// A pointer stores the address of a
	// variables data in memory
	int val = 43;
	// Store the address by placing the
	// reference operator before the variable
	int* pVal = &val;
	// Get the address
	writeln("Address : ", pVal);//7FFFFFFFDAC4
	// Get the value stored there
	writeln("Value : ", *pVal);//43
	// Pass a pointer to a function
	void changePtrVal(int* pVal){
      *pVal = 22;
    }
	changePtrVal(&val);
	writeln("Value : ", val);//2

	double[] kk=[1,2,3];
	double * pkk=&kk[0];
	pkk[1]=4;
	
	int[] mm=[1,2,3];
	int * amm=&mm[0];
	int * pmm=mm.ptr;
	writeln(amm); //8010011B0,
	writeln(pmm); //8010011B0,Same address

//POINTER--------------------------------------------------
	//Stack pointer
	int aa1;
	int* pb = &aa1; // b contains address of a
	auto pc = &aa1; // c is int* and contains address of a
	writeln("a ", aa1);//0
	writeln("b ", pb);//7FFFFFFFDA7C
	writeln("c ", pc);//7FFFFFFFDA7C
	
	//Heap pointer
	int* dd = new int;
    	*dd = 42; // dereferencing
    	writeln("a: ", *dd);//42
	
	int *y1;      // default initalised to null;
	int *y2=null; // explicit
	int* y3,y4;   // y4 is a pointer

	int x4= 42;
	// Pointer to the stack, used for small data
	int* px= &x4;
	++(*px);
	int[] ar=[1,2,3,4];
	int * pp=ar.ptr;
	++pp;
	*pp=10;
	
	//Pointer to the heap, used for large data
	//heap is managed by garbage collector !!!!!!!!!!!!!!!!!!!!!!!!!
	int *h1=new int;
	
	//
	int [] z3=[1,2,3];
	int *zp1=&z3[0];
	int *zp2=z3.ptr;
	int *zp3=(new int[] (3)).ptr;
	int *zp4=(new int[3]).ptr;

	int[2]  ax=new int[2];
	int[]   bx=new int[0];
	int[2]  cxz=[1,2];
	// ax=ax~3;        Illegal
	bx=bx~3;
	// cx=cx~3;        Illegal 
	// cx=bx;          ????? ILLEGAL, WHY ????????
	bx=ax;
	ax=cxz;


	const int MAX = 7;

	int [MAX] var = [0,1,2,3,4,5,6]; 
    var.ptr[2]  = 290; 
    int *ptr = &var[2]; 
    ptr[1]=100;  
    ptr++;
    ptr[2]=1000;
	writeln(var);//[0, 1, 290, 100, 4, 1000, 6]
	writeln(typeid(var));        //int[7]
	writeln(typeid(var.ptr));    //int*
	writeln(typeid(var.ptr[0])); //int
	writeln(typeid(ptr));        //int*

//ASSOCIATIVE ARRAY--------------------------------------
    // value_type[key_type] associative_array_name;
	// Key value pairs
	double[string] favNums =["AZero" : -459, "Euler" : 2.7182];
	// Add a key / value
	favNums["PI"] = 3.14159;
	favNums["Golden"] = 1.61803;
	// Get values
	writeln(favNums);//["Euler":2.7182, "PI":3.14159, "Golden":1.61803, "AZero":-459]
	writeln(favNums["Golden"]);//1.61803
	// Number of values
	writeln(favNums.length);//4
	// Get keys and values
	writeln(favNums.keys);//["Euler", "PI", "Golden", "AZero"]
	writeln(favNums.values);//[2.7182, 3.14159, 1.61803, -459]
	// Check if key exists and delete
	if("AZero" in favNums)
        {favNums.remove("AZero");}
	writeln(favNums);//["Euler":2.7182, "PI":3.14159, "Golden":1.61803]

	//Associative arrays
	int  [immutable(char)[]] xa=[ "Alain":10 , "Eddy":5];
	writeln(typeid(typeof(xa)));//int[immutable(char)[]]
	immutable(char)[]  [int] ya=[10:"Alain",5:"Eddy"];
	writeln(typeid(typeof(ya)));//immutable(char)[][int]

	double[string] table;
	table["Alain"]=5;
	writeln(table.length);//1

	//In , determine presence 
	double* pa = "Alain" in table;
	if(pa) writeln(table["Alain"]);//5
	if(pa) writeln(*pa); //writes 5

	//Get a copy of keys and values
	string[]   s5=table.keys;
	double []  d3=table.values;

	int [string] aaz=["Alain":10,"Eddy":5];
	aaz.remove("Alain");
	aaz["Alain"]=10;
	aaz["Alain"]=15; // overwrite key
	aaz["Eddy"]=20;
	foreach( key,value  ; aaz) 
		writefln("%s key %s value",key,value); //Eddy 20 Alain 15
	foreach(value;aaz) writeln("Value:",value);//20 15
	foreach (key; aaz.byKey) { 
      	writeln("by key: ",key); //Eddy Alain
   	}
	foreach (kv; aaz.byKeyValue) { 
		writefln("Key: %s, Value: %d" ,kv.key,kv.value);//Eddy 20 Alain 15 
	}

	int[string] array1;

	array1["test"] = 3; 
	array1["test2"] = 20; 
   
	writeln("sizeof: ",array1.sizeof);//8
	writeln("length: ",array1.length);//2
	writeln("dup: ",array1.dup);//["test":3, "test2":20]
 	array1.rehash; 

	writeln("rehashed: ",array1);//["test":3, "test2":20]  
	writeln("keys: ",array1.keys);//["test","test2"]
	writeln("values: ",array1.values);//[3,20]
   
	foreach (key; array1.byKey) { 
		writeln("by key: ",key);//test test2
	}

	foreach (value; array1.byValue) { 
		writeln("by value ",value);//3 20 
	}

	writeln("get value for key test: ",array1.get("test",10));//3
	writeln("get value for key test3: ",array1.get("test3",10));//10  
	array1.remove("test"); 
	writeln(array1);//["test2":20]
}


