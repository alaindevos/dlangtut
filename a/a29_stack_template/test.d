import std.stdio: writeln;

template IRange(T) {
struct   IRange{
    T [] _elements;
    bool empty() { return _elements.length == 0; }
    T front() { return _elements[$-1]; }
    void popFront() { _elements.length -= 1; }
}
}

template IStack(T) {
struct   IStack{

    private T [] _array;

    void push(T element) {
        _array ~= element;
    }

    void pop() {
        assert(!isEmpty);
        _array.length -= 1;
    }
    ref int top() {
        assert(!isEmpty);
        return _array[$-1];
    }
    bool isEmpty() { return _array.length == 0; }

    auto elements() { return IRange!(T)(_array); }
}
}

void main(){
    IStack!(int) stack;
    foreach(i; 0..5) stack.push(i);
    writeln("Iterating...");
    foreach(i; stack.elements) writeln(i);
    stack.pop();
    stack.pop();
    writeln("Iterating...");
    foreach(i; stack.elements) writeln(i);
}

