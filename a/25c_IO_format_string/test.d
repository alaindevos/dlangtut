import std.format:format;
import std.stdio:writeln,writefln,File;
import std.range:iota;
import std.algorithm:map;

void main(){

//format int
int value = 12;
writefln("Binary: %b", value);
writefln("Octal: %o", value);
writefln("Hexadecimal: %x", value);
writefln("Decimal: %d", value);

//format double
double valuew = 123.456789;
writefln("with e: %e", valuew); //1.234568e+02
writefln("with f: %f", valuew); //123.456789
writefln("with g: %g", valuew); //123.457
writefln("with a: %a", valuew); //0x1.edd3c07ee0b0bp+6

//format type
bool b = true;
int i = 365;
double d = 9.87;
string s = "formatted";
auto o = File("test.html", "r");
int[] a = [ 2, 4, 6, 8 ];
writefln("bool : %s", b);
writefln("int  : %s", i);
writefln("double: %s", d);
writefln("string: %s", s);
writefln("object: %s", o);
writefln("array : %s", a);

//width
int valuez = 100;
writefln("In a field of 10 characters:%10s", valuez);

//separator
writefln("%,f", 1234.5678);     // Groups of 3   1,234.567,800
writefln("%,s", 1000000);       // Groups of 3   1,000,000
writefln("%,2s", 1000000);      // Groups of 2   1,00,00,00
writefln("%,*s", 1, 1000000);   // Groups of 1   1,0,0,0,0,0,0
writefln("%,?s", '.', 1000000); //1.000.000

//precision
double valuee = 1234.56789; 
writefln("%.8g", valuee);   // 1234.5679
writefln("%.3g", valuee);   // 1.23e+03
writefln("%.8f", valuee);   // 1234.56789000
writefln("%.3f", valuee);   // 1234.568

auto number = 0.123456789;
writefln("Number: %.*g", 4, number); // 0.1235

//flags
int valueb = 123;
writefln("Normally right-aligned:|%10d|", valueb);
writefln("Left-aligned:|%-10d|", valueb);

//format input/output
// Formatting for integer types uses %d
writeln("byte min : ", byte.min);
writeln("byte max : ", byte.max);//127
writeln("short min : ", short.min);
writeln("short max : ", short.max);//32767
writeln("int min : ", int.min);
writeln("int max : ", int.max);//2147483647
writeln("long min : ", long.min);
writeln("long max : ", long.max);//9223372036854775807
// Shift right depending on the output
writeln("float max : ", float.max);//3.40282e+38
float fNum = 1.1111111111111111;
float fNum2 = 1.1111111111111111;
// Formats output to show 16 decimals
// Floats are accurate to 6 decimals
writefln("Float : %1.16f", fNum + fNum2);
// Doubles are accurate to 15
writeln("double max : ", double.max);//1.79769e+308
double dNum = 1.1111111111111111;
double dNum2 = 1.1111111111111111;
writefln("Double : %1.16f", dNum + dNum2);
// Doubles are accurate to 19
writeln("real max : ", real.max);//1.18973e+4932
real rNum = 1.11111111111111111111;
real rNum2 = 1.11111111111111111111;
writefln("Real : %1.20f", rNum + rNum2);
// Get default values
writeln("Default int : ", int.init);

auto s2="Hi";
auto s3="Dear";
//Only runtime check
//format uses gc heap
auto s1=format("Hello %s %s",s2,s3);
writeln(s1);

//Compile-time safe
s1=format!"Hello %s %s"(s2,s3);
writeln(s1);

// %( open %) close %s element
5.iota.writefln!"%( %s,%)";

//Prints: 0,1,2,3,4     !!!!
5.iota.writefln!"%(  <%s> %| %)  ";  
//Prints: <0> <1>  <2> <3> <4>

// %-( : don't print quotes
["monday","tuesday"].writefln!"%-(%s,%)";
//Prints : monday,tuesday

//Print triangle of numbers
5.iota.map!(i=>i.iota).writefln!"%( %( %s, %) \n %)";

//Print key-value
auto aa=["Alain":"Vijf","Eddy":"Zeven"];
writeln(typeid(typeof(aa)));         //immutable(char)[][immutable(char)[]]
aa.writefln!"%(%s is %s\n%)";        //Eddy is Zeven Alain is Vijf ..Weird...

//Decimal place separator %,
writefln!"%,s"(123456789);
//123,456,789

//*s
writefln!"%,*s"(6,123456789);
//123,456789

//?s
writefln!"%,?s"('*',123456789);
//123*456*789

//*?s
writefln!"%,*?s"(2,'*',123456789);
//1*23*45*67*89

// pad length 10
auto f="%10s"; 
writefln(f,123);

// length 10, left justify
auto g="%-10s";
writefln(g,123);

auto sr = format!"%,s"("123456789");
writeln(sr);



}
