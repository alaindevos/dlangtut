import std.algorithm: map,each,filter,fold;
import std.algorithm.comparison: max;
import std.algorithm.iteration: reduce,map;
import std.array: array;
import std.datetime.systime: Clock;
import std.datetime.timezone: UTC;
import std.random: Random,uniform;
import std.stdio: writeln;
import std.range: iota,chain,generate,take;

void main(){
	
// ---- ANONYMOUS FUNCTIONS &-----------------------------------------
// SEQUENCE PROCESSING ----
// Allow you to define a function inside
// an expression

	auto aF1 = (int a) => a * 2;
	writeln(aF1(5));//10
// Use map to multiply all by 2
	int[] a7 = [1,2,3];
	auto aF2 = map!(a => a * 2)(a7);
	writeln(aF2);//[2,4,6]
    // Combine arrays and do the same
    int[] a8 = [5,6,7];
    auto aF3 = map!(a => a * 2)(chain(a7,a8));
    writeln(aF3);//[2,4,6,10,12,14]
    // Use filter to get evens
    auto aF4 = a7.filter!(a => (a % 2) == 0);
    writeln(aF4);//[2]
    // Use reduce to add values
    auto aF5 = reduce!((a,b) => a + b)(0,a7);
    writeln(aF5);//6
    // Get max
    auto aF6 = reduce!(max)(aF3);
    writeln(aF6);//14
     // Generate a random range
    auto now2 = Clock.currTime(UTC());
    auto seed2 = now2.second;
    auto rand2 = Random(seed2);
    auto randVals = generate!(() => uniform(1, 2000, rand2))().take(10);
    foreach(x; randVals)
      writeln(x); //randoms
    writeln("V");
	bool mywriteln(int x){
		writeln(x);
		return true;
	}

//MAP-----------------------------------------
	5.iota.map!mywriteln.array;//0,1,2,3,4
	writeln("------------------");
	5.iota.map!mywriteln.each;//0,1,2,3,4
	writeln("------------------");
	5.iota.each!writeln;//0,1,2,3,4

	auto rin=[3,4,5];
	auto rout=rin.map!(x=>x*2);
	foreach(n;rout) writeln(n);//6,8,10

//FILTER----------------------------------------------
	auto fil=rin.filter!(value=> value>3);
	foreach(n;fil) writeln(n);//4,5

//REDUCE----------------------------------------------
	int [] r=[1,2,3];
	int sum1=reduce!(
		(a,b){return a+b;}
	)(0,r);
	writeln(sum1);//6
	int sum2=reduce!(
		(a,b)=>a+b
	)(0,r);
	writeln(sum2);//6
	int min=reduce!(
		(a,b){return a < b ? a : b ;}
	)(r[0],r);
	writeln(min);//1
	

//FOLD----------------------------------------
	immutable arr = [0, 1, 2, 3];
	// Sum all elements
	writeln(arr.fold!((a, b) => a + b)); //6
	
}

