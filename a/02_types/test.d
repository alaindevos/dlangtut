import std.algorithm: remove,reverse;
import std.algorithm.sorting: sort;
import std.array: Appender,replace,split;
import std.complex: complex;
import std.conv: octal,to;
import std.datetime: Date,TimeOfDay;
import std.datetime.date: DateTime;
import std.stdio: writeln,writefln;
import std.typecons:tuple,Tuple,No;
import std.uni: isLower,isUpper,isAlpha,isWhite,toLower,toUpper;
import std.variant: Variant;
import std.string: indexOf,lastIndexOf,isNumeric,strip;


void main(){

//Skip initalization
	int dog = void; // dog contains garbage
//BOOL------------------------------------------------------
	// You can have the type picked for you
	auto bool1 = true;
	writeln("Bool : ", bool1);//true 
	// booleans are either true or false
	// Formatted with %s
	writeln("int min : ", bool.min);//false
	writeln("int max : ", bool.max);//true
	bool happy = true;
	writefln("Bool : %s", happy);//true

	bool bo1=false;
	bool bo2=0 ; //false
	bool bo3=cast(bool) 40 ;//true


//BOOL-OPERATOR
	auto bof= (1 !=2) ; 	//Not equal
	auto bof2 = ( true && false); 	//And

//INTEGER-NUMBER------------------------------------------------------------
	//identifier ; variable declaration and initalization ; number---------
	// You can use underscores
  	// ubyte, ushort, uint, ulong,
  	// Imaginary : ifloat, idouble, ireal
  	// Complex : cfloat, cdouble, creal
    //8 bit
	ubyte ub=0;
	byte b=0;

    //16bit
	ushort us=0;
	short s=0;

    //32bit
	uint ui=0;
	int i1=0;
	int i2=0xFF  ;     //hex literal  
	int i3=0b00001111; //binary literal
	int i4=octal!377;  //octal literal
	int  x1= false ; //0
	int  x2= true ; //1
	int hhh=1_234_567;
  	int a1_B = 10;

    //64bit
	ulong ul=0UL; //Unsigned Long
	long l=0L;    //Long
	long lNum = 123_456_789_100;

//Properties
	writeln(int.max);    //2147483647
	writeln(long.max);   //9223372036854775807
	writeln(float.max);  //3.40282e+38
    writeln("Type: ", float.stringof); //float

//INT-OPERATORS
    int xxx=1;
	++xxx; 	//Prefer prefix notation code
    xxx+= 10; 
	xxx=3 ^^4  ; //power
	xxx=17 % 3 ; //modulus
	xxx= 1 << 4 ; 	//Bit shift
	writeln(xxx); //16

//FLOATINGPOINT-NUMBER---------------------------------------------------

    //32-bit
	float f=float.nan;
	f=0f;

    //64-bit
	double d=double.nan;
	d=0f;

    //At-least-64-bit
	//Real : Architecture dependend
	//Float a double calculations are done in real 
	real r=real.nan;

//Properties
	writeln(double.max); //1.79769e+308

//Complex
    auto cccc = complex(1.0, 2.0);    


//CHAR-------------------------------------------------------------
	

    //char
	char cx=0xFF;    //UTF-8
	cx='c';
    cx='\n';//newline
    cx='\r';//carriage return
    cx='\t';//tab
    cx='\'';//single quote
    cx='\\';//backslash

	wchar wc=0xFFF; //UTF-16
	wc='\u1111';//unicode
    wc='é';//literal
    wc = '\&euro;';//shortname

	dchar dcc=0x0000FFFF; //UTF-32
	dcc='\U00008888';
	string dg="I said \"Hello, world!\""; 	//ESCAPED quotes \" is not the end
	string ry=`Hello, world!`;  //raw wysiwig string
	string rz=r"Hello, world!"; //raw wysiwig string

	// Strings are treated like dynamic arrays with variable length

//Function  	
	// chars are single characters surrounded with single quotes
	// Can also store \n, \t, \b, \f, \r, \", \', 
	writeln("char min : ", char.min);
	writeln("char max : ", char.max);
	char aq = 'a';
	writefln("Char : %c", aq);//a

	
	//std.uni;
	bool bo5=isLower('a');
	bool bo6=isUpper('b');
	bool bo7=isAlpha('c');
	bool bo8=isWhite('d');
	char cc1=cast(char)toLower('A');
	char ccd=cast(char)toUpper('a');

//SIZE_T,ALIAS
	size_t t1; // size_t : offset to addressable memory
	//alias substitutes a symbol
	//Gives a different name to an existing type.
	alias typeof(char.sizeof) my_size_t; //typeof sizeof is unsigned long
	my_size_t  t2;
	writeln(size_t.max);      // 18446744073709551615
	writeln(my_size_t.max);   // 18446744073709551615
	
	ptrdiff_t d1;
	alias long my_ptrdiff_t;
	my_ptrdiff_t d2;
	writeln(ptrdiff_t.max);     //9223372036854775807
	writeln(my_ptrdiff_t.max);	//9223372036854775807

	void fun(){
	}
	alias fun gun;

//ENUM-----------------------------------------------------------------------------
	// Enums are immutable constants; they subsitute their literals
	// Named enums create a new type
	// The name of a named enum forms a namespace and members are accessed by .
	// Enum of ints, starting at zero

    // Custom type made up of named constants
	enum Suit {club, diamond, heart, spade}
	Suit card = Suit.heart;
	writeln(card);//heart

	enum Names { alain , eddy }
	Names x;
	assert(x==Names.alain); // Initializer is first word
	int y=x ; // Conversion to int

	//Enum of ubyte ,starting at zero
	enum Test : ubyte { Pol ,Frans}

	//enums of the same type
	enum Names2:string { one="One", two="Two" }

	// Enum of a struct , Enums of classes do not exist
	struct Color {
		ubyte r,g,b;
	}
	enum Colors { red=Color(255,0,0) , green=Color(0,255,0) }

	//Anonymous enum
	enum { Jan , Piet }

	// Anonymous enum with one member;
	enum int aaa= 23; //startvalue
	enum     bbb;
	enum     ccc= "Hallo";	
	
	writeln(Names2.init);        // "One" i.e. first element
	writeln(Names.min);          // "alain", first element
	writeln(typeid(Colors.red)); // test.main.Colors
	
	// Final Switch
	Test c=Test.Pol;
	final switch(c) {
		case Test.Pol   : writeln("red")  ; break;          //print red
		case Test.Frans : writeln("green"); break;
	} 
    

// ---- STRINGS --------------------------------------------------------
	// Strings are arrays of characters but
	// the character values can't be changed
    // They are immutable
	string s1 = "Doug";
	// s1[0] = 'T';  // Would throw an error

    // Mutable "string"
    char [] muts="hello".dup;
    muts[0]='H';

	// You can get the character
	writeln(s1[0]);//D
	// You can change the string in whole
	s1 = "Tom";
	// Get size
	writeln("Size : ", s1.length);//3
	// Combine strings
	string s2 = s1 ~ " Thumb";
	// Find index
	writeln("T : ", indexOf(s2, 'T'));//0
	writeln("Last T : ", lastIndexOf(s2, 'T'));//4
	writeln("Thumb : ", indexOf(s2, "Thumb", No.caseSensitive));//4
	// Find and replace
	writeln(replace(s2, "Tom", "Ed"));//Ed Tumb
	// Convert string to array
	string s3 = "1 2 3";
	auto numArr = to!(int[])(split(s3));
	writeln(numArr);//[1,2,3]
	// Check equality
	string s4 = "1 2 3";
	writeln(s3 == s4);//true
	// Convert upper and lower
	writeln(toUpper(s2));//TOM THUMB
	writeln(toLower(s2));//tom thumb
	// Is it a number
	writeln(isNumeric("10"));//true

	//string
	string sx2="\r\n\t";  //control characters
	string sx3="\'\"\\"; // single quote,double quote, backslash
	string sx4=r"'''''''";  //WYSIWYG string
	string rrr=r"WYSIWYG string"; 	//WYSIWYG string
	string qqq=q"<blabla>";	//Delimited string  <> is not part of it.


	class Base{}

	Base bb=new Base();
	writeln(typeid(typeof(bb))); // Prints test.main.base

	string sc="123"c;//UTF-8
	wstring sw="123"w;//UTF-16
	dstring sd="123"d;//UTF-32

	//Length
	writeln("123".length);  //3

	//Indexing
	string xh="abc";
	foreach( char cy;xh)writeln(cy);  // a b c

	// drop The x !
	alias  stringx=immutable(char)[];
	alias wstringx=immutable(wchar)[];
	alias dstringx=immutable(dchar)[];


//STRING-OPERATOR
	string af="aaa";
	string bf="bbb";
	string cf=af~bf; 	//CONCAT
    string sstrip="  aaaaa  ".strip(); // Remove whitespace at both ends
    bool mybefore = ( "aaa" < "bbb");


//DATE------------------------------------------------------
	auto date = Date(1992, 12, 27);
	auto tod = TimeOfDay(7, 0, 22);
	auto dateTime = DateTime(1992, 12, 27, 7, 0, 22);
	writeln(dateTime.toISOExtString());// 1992-12-27T07:00:22

//VARIANT---------------------------------------------------
	Variant v=1;
	v=1L;
	v="Hallo";

//OTHER------------------------------------------------------
	void * v3;
	const e4=42 ; // Type is infered by compiler
	auto  f5=43 ; // Auto mains automatic storage class
    int   ia=1 ;
    typeof(ia) ib=1; //int
    alias typeof(ia) ta;
    ta ic=1;

//UNION------------------------------------------------------
	// A union is a struct which all members start at the same address
	// Initialized to it first member
	union intorfloat{
		int myint = 23 ;
		float myfloat;
	}

	intorfloat x3;
	x3.myint=1;
	x3.myfloat=5.5;

    struct TaggedUnion{
	    enum Tag { Tint, Tdouble, Tstring}
	    private Tag atag;
	    private union {
		    int anint;
		    double adouble;
		    string astring;
	    }//union
    }//struct TaggedUnion
    TaggedUnion ttt=TaggedUnion(TaggedUnion.Tag.Tint,0);

//TUPLE----------------------------------------------------------
	// ---- TUPLES --------------------------------------------------------
	// Used to store a key/value at once
	auto t8 = tuple("A",1);
	writeln(t8); //Tuple!(string, int)("A", 1)
	// You can store more than 2 values
	auto t9 = tuple("B", 2, 5.6);
	writeln(t9);//Tuple!(string, int, double)("B", 2, 5.6)
	// Change values
	t9[0] = "A";
	// Get values by index
	writeln(t9[0]);//A
	// You can pass multiple values into
	// functions
	void printTuple(string s, int i){
	writeln(s, " ", i);//A 1
	}
	printTuple(t8.expand);

	// You can also return multiple values
	Tuple!( int, string ) returnTuple(){
	return tuple(5, "Hello");
	}
	auto t3 = returnTuple();
	writeln(t3);//Tuple!(int, string)(5, "Hello")

	Tuple!(int, string) t5=tuple(42,"Hallo");
	writeln(t5);         // Tuple!(int, string)(42, "Hallo")
	writeln(typeid(t5)); //std.typecons.Tuple!(int, string).Tuple

	auto t6=Tuple!(int,"number",string,"name")(42,"Alain");
	writeln(t6.number,t6.name); //42Alain
	writeln(typeid(t6));         //std.typecons.Tuple!(int, string).Tuple
}
