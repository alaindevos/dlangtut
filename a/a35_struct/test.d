import std.stdio:writeln;
import std.range:take;
import std.typecons:tuple;
import std.algorithm.iteration:map,filter;
import std.algorithm.sorting:sort;

struct User {
    int id;
    string name;
    string title;
}

void main(){
    User[] users;
    users~= User(1,"Alain","President");
    users~= User(3,"AJan","It");
    users~= User(2,"AEddy","Manager");
    auto res1=users.sort!((a,b)=>(a.id>b.id));
    auto res2=res1.take(2);
    auto res3=res2.map!((item)=>tuple(item.id,item.name));
    foreach (line;res3)
        writeln(line[0]," ",line[1]);
}
