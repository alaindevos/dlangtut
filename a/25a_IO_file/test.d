import std.stdio: writeln,readln,File;
import std.string: chomp,strip;
import std.file:exists;
//std.stdio: package
//File: module
//Open: function
  
void main() { 

string filename="test.txt";

//files----------------------------------------------------------------
// ---- FILE IO ----
// Open a file for writing
// w : Open for writing and/or create file
// a : Open for appending and/or create file
// r : Open for reading
// r+ : Open for reading and writing
// w+ : Reading and writing, 0 file, or create
// a+ : Reading and append writing

//Write
File file = File(filename, "w");
file.writeln("Some random text");
file.writeln("More random text");
file.close();

//Exists
if(filename.exists)
	writeln(filename," exists");

//Append
file.open(filename,"a");
file.write("Hallo\n");
file.close();
 
// Read
file = File(filename, "r");
// As long as their are lines print them
while(!file.eof()){
	string line=file.readln().strip;
	writeln("Line:",line);
	}
file.close();

}
