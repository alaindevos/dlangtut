module fibonacci;
import std.stdio;
ulong fib(ulong x)
{
return x <2 ? x : fib(x-2)+fib(x-1);
}
int main()
{
writefln("%s, ", fib(40));
return 0;
}

