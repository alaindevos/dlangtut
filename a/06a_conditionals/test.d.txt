[01;34mimport[m std[31m.[mstdio[31m:[mwriteln[31m,[mwritefln[31m;[m

[01;34mvoid[m [01;30mmain[m [31m()[m [31m{[m 

[31m// Logical Operators : && || ![m
 	[01;30mwriteln[m[31m([m[31m"true || false = "[m[31m,[m [31m([m[01;34mtrue[m [31m||[m [01;34mfalse[m[31m));[m
	[01;30mwriteln[m[31m([m[31m"!true = "[m[31m,[m [31m(![m[01;34mtrue[m[31m));[m
    [32mbool[m myequal [31m=[m [31m([m [35m2[m [31m==[m [35m2[m[31m);[m
    [32mbool[m my_xor [31m=[m [31m([m[01;34mtrue[m [31m^[m [01;34mfalse[m[31m);[m
[31m// Relational Operators : > < >= <= == !=[m

[31m//TERNARY[m
	[31m// The ternary operator returns the 1st value[m
	[31m// when the condition is true and the 2nd[m
	[31m// otherwise[m
    [32mint[m age[31m=[m[35m14[m[31m;[m
	[32mbool[m canVote [31m=[m [31m([mage [31m>=[m [35m18[m[31m)[m [31m?[m [01;34mtrue[m [31m:[m [01;34mfalse[m[31m;[m
	[01;30mwriteln[m[31m([m[31m"Can Vote : "[m[31m,[mcanVote[31m);[m

	[37mstring[m s[31m=[m [31m([m[35m1[m[31m==[m[35m1[m[31m)[m [31m?[m [31m"gelijk"[m [31m:[m [31m"ongelijk"[m[31m;[m
	[01;30mwriteln[m[31m([ms[31m);[m
	
	[37mstring[m isfour [31m=[m [31m([m[35m2[m[31m+[m[35m2[m[31m==[m[35m4[m[31m)[m [31m?[m [31m"Isfour"[m [31m:[m [31m"Is not four"[m[31m;[m
	[01;30mwriteln[m[31m([misfour[31m);[m

[31m//IF-ELSE	[m
	[01;34mif[m [31m(([mage [31m>=[m [35m5[m[31m)[m [31m&&[m [31m([mage [31m<=[m [35m6[m[31m))[m[31m{[m
		[01;30mwriteln[m[31m([m[31m"Go to Kindergarten"[m[31m);[m
	[31m}[m [01;34melse[m [01;34mif[m [31m(([mage [31m>=[m [35m7[m[31m)[m [31m&&[m [31m([mage [31m<=[m [35m13[m[31m))[m[31m{[m
		[01;30mwriteln[m[31m([m[31m"Go to Middle School"[m[31m);[m
	[31m}[m [01;34melse[m [01;34mif[m [31m(([mage [31m>=[m [35m14[m[31m)[m [31m&&[m [31m([mage [31m<=[m [35m18[m[31m))[m[31m{[m
		[01;30mwriteln[m[31m([m[31m"Go to High School"[m[31m);[m
	[31m}[m [01;34melse[m [31m{[m
		[01;30mwriteln[m[31m([m[31m"Stay Home"[m[31m);[m
	[31m}[m

	[32mint[m a [31m=[m [35m10[m[31m;[m 
	[01;34mif[m     [31m([m a [31m==[m [35m9[m  [31m)[m [01;30mwriteln[m[31m([m[31m"a is  9"[m [31m);[m
	[01;34melse[m [01;34mif[m[31m([m a [31m==[m [35m10[m [31m)[m [01;30mwriteln[m[31m([m[31m"a is 10"[m [31m);[m
	[01;34melse[m               [01;30mwriteln[m[31m([m[31m"a is not 9 or 10"[m[31m);[m


	[01;34mif[m [31m([m[35m1[m[31m>[m[35m2[m[31m)[m
		[01;30mwriteln[m[31m([m[31m"A"[m[31m);[m
	[01;34melse[m [01;34mif[m [31m([m[35m2[m[31m>[m[35m3[m[31m)[m
		[01;30mwriteln[m[31m([m[31m"B"[m[31m);[m
	[01;34melse[m
		[01;30mwriteln[m[31m([m[31m"C"[m[31m);[m

[31m//SWITCH[m
	[31m// Switch works great with limited options[m
	[37mstring[m lang [31m=[m [31m"France"[m[31m;[m
	[01;34mswitch[m[31m([mlang[31m)[m[31m{[m
		[01;34mcase[m [31m"Chile"[m[31m:[m [01;34mcase[m [31m"Cuba"[m[31m:[m
			[01;30mwriteln[m[31m([m[31m"Hola"[m[31m);[m
			[01;34mbreak[m[31m;[m
		[01;34mcase[m [31m"France"[m[31m:[m
			[01;30mwriteln[m[31m([m[31m"Bonjour"[m[31m);[m
			[31m// Use goto to check the next[m
      [01;34mgoto[m [01;34mcase[m[31m;[m
		[01;34mcase[m [31m"Japan"[m[31m:[m
			[01;30mwriteln[m[31m([m[31m"Konnichiwa"[m[31m);[m
			[01;34mbreak[m[31m;[m
		[01;34mdefault[m[31m:[m
			[01;30mwriteln[m[31m([m[31m"Hello"[m[31m);[m
		[31m}[m
	[31m// You can also use ranges[m
	[32mint[m anAge [31m=[m [35m7[m[31m;[m
	[01;34mswitch[m[31m([manAge[31m)[m[31m{[m
		[01;34mcase[m [35m7[m[31m:[m [31m..[m [01;34mcase[m [35m13[m[31m:[m
			[01;30mwriteln[m[31m([m[31m"Go to middle school"[m[31m);[m
			[01;34mbreak[m[31m;[m
		[01;34mdefault[m[31m:[m
			[01;30mwriteln[m[31m([m[31m"Somewhere else"[m[31m);[m
	[31m}[m

	[32mint[m expression[31m=[m[35m1[m[31m;[m	
	[01;34mswitch[m [31m([mexpression[31m)[m [31m{[m
		[01;34mcase[m [35m0[m[31m:[m  [01;30mwriteln[m[31m([m[31m"0"[m[31m);[m[01;34mbreak[m[31m;[m
		[01;34mcase[m [35m1[m[31m:[m  [01;30mwriteln[m[31m([m[31m"1"[m[31m);[m[01;34mbreak[m[31m;[m
    		[01;34mdefault[m[31m:[m [01;30mwriteln[m[31m([m[31m"other"[m[31m);[m[01;34mbreak[m[31m;[m
    [31m}[m

   	[32mint[m xx[31m=[m[35m2[m[31m;[m
	[01;34mswitch[m[31m([mxx[31m)[m [31m{[m
		[01;34mcase[m [35m1[m[31m:[m
			[01;30mwriteln[m[31m([m[31m""[m[31m);[m
			[01;34mbreak[m[31m;[m
		[01;34mcase[m [35m2[m[31m:[m
			[01;30mwriteln[m[31m([m[31m""[m[31m);[m
			[01;34mbreak[m[31m;[m
		[01;34mcase[m [35m3[m[31m:[m
		[01;34mcase[m [35m4[m[31m:[m
			[01;30mwriteln[m[31m([m[31m""[m[31m);[m
			[01;34mbreak[m[31m;[m
		[01;34mdefault[m[31m:[m
			[01;30mwriteln[m[31m([m[31m""[m[31m);[m
			[01;34mbreak[m[31m;[m
	[31m}[m
	

	
[31m//GOTO-BREAK[m
	[32mint[m b[31m=[m[35m1[m[31m;[m
	[01;34mgoto[m MYLABEL[31m;[m
	b[31m=[m[35m2[m[31m;[m

[01;37mMYLABEL:[m
	[01;30mwriteln[m[31m([mb[31m);[m
	

	[32mint[m [31m[][m items[31m=[[m[35m1[m[31m,[m[35m2[m[31m,[m[35m3[m[31m];[m
	[32mint[m t[31m=[m[35m0[m[31m;[m


[01;37mMYBREAK:[m
	[01;34mwhile[m[31m([m[01;34mtrue[m[31m)[m
	[31m{[m
	 [01;34mif[m[31m([mt[31m==[mitems[31m.[mlength[31m)[m[01;34mbreak[m MYBREAK[31m;[m
	 items[31m[[mt[31m++].[mwriteln[31m;[m
	[31m}[m
	
[31m//FOR-------------------------------------------[m
[31m// For loop[m
    [01;34mfor[m[31m([m[32mint[m i [31m=[m [35m0[m[31m;[m i [31m<[m [35m5[m[31m;[m i[31m++)[m[31m{[m
      [01;30mwriteln[m[31m([mi[31m);[m
    [31m}[m

	[32mint[m c[31m=[m[35m1[m[31m;[m
	[01;34mfor[m[31m(;;)[m[31m{[m
		c[31m=[mc[31m+[m[35m1[m[31m;[m
		[01;34mif[m [31m([mc[31m==[m[35m10[m[31m)[m [01;34mbreak[m[31m;[m
	[31m}[m
	[01;30mwriteln[m[31m([mc[31m);[m

	[01;34mfor[m[31m([m[32mint[m t2[31m=[m[35m0[m[31m;[mt2[31m<[mitems[31m.[mlength[31m;++[mt2[31m)[m
		items[31m[[mt2[31m].[mwriteln[31m;[m


[31m//WHILE[m
    [31m// While executes as long as a condition[m
    [31m// is true[m
    [32mint[m wI [31m=[m [35m0[m[31m;[m
		[01;34mwhile[m [31m([mwI [31m<[m [35m20[m[31m)[m [31m{[m
			[31m// Only print even numbers[m
			[01;34mif[m[31m([mwI [31m%[m [35m2[m [31m==[m [35m0[m[31m)[m [31m{[m
				[01;30mwriteln[m[31m([mwI[31m);[m
				wI[31m++;[m
				[31m// Jump back to the beginning of loop[m
				[01;34mcontinue[m[31m;[m
			[31m}[m
			[01;34mif[m[31m([mwI [31m>=[m [35m10[m[31m)[m [31m{[m
				[31m// Stop looping[m
				[01;34mbreak[m[31m;[m
			[31m}[m
			wI[31m++;[m
		[31m}[m
		
	[01;34mwhile[m[31m([mt[31m<[mitems[31m.[mlength[31m)[m
		items[31m[[mt[31m++].[mwriteln[31m;[m

[31m//DO-WHILE[m
	[31m// Do While always executes once[m
	[31m// int secretNum = 7;[m
	[31m// int guess = 0;[m
	[31m// do {[m
		[31m// 	write("Guess : ");[m
		[31m// 	readf("%d\n", &guess);[m
		[31m// }while(secretNum != guess);[m
		[31m// writeln("You guessed it");[m

	t[31m=[m[35m0[m[31m;[m
	[01;34mdo[m[31m{[m
		items[31m[[mt[31m++].[mwriteln[31m;[m
		[31m}[m	[01;34mwhile[m[31m([mt[31m<[mitems[31m.[mlength[31m);[m


[31m//FOREACH[m
	[31m// foreach is more commonly used[m
	[31m// for containers[m
	[32mint[m[31m[][m a6 [31m=[m [31m[[m[35m1[m[31m,[m[35m2[m[31m,[m[35m3[m[31m,[m[35m4[m[31m];[m
	[01;34mforeach[m[31m([mx[31m;[m a6[31m)[m[31m{[m
		[01;30mwriteln[m[31m([mx[31m);[m
	[31m}[m
	[31m// Work with ranges[m
	[01;34mforeach[m[31m([mx[31m;[m [35m5[m[31m..[m[35m10[m[31m)[m[31m{[m
		[01;30mwriteln[m[31m([mx[31m);[m
	[31m}[m

	[31m// Associative Arrays foreach loop[m
    [32mdouble[m[31m[[mstring[31m][m aA [31m=[m
    [31m[[m[31m"A"[m [31m:[m [35m1[m[31m,[m
    [31m"B"[m [31m:[m [35m2[m[31m];[m
    [31m// Keys and values[m
    [01;34mforeach[m[31m([mk[31m,[m v[31m;[m aA[31m)[m[31m{[m
      [01;30mwriteln[m[31m([mk[31m,[m [31m" "[m[31m,[m v[31m);[m
    [31m}[m
    [31m// Get both as a group[m
    [01;34mforeach[m[31m([mx[31m;[m aA[31m.[mbyKeyValue[31m)[m[31m{[m
      [01;30mwritefln[m[31m([m[31m"%s : %s"[m[31m,[m
      x[31m.[mkey[31m,[m x[31m.[mvalue[31m);[m
    [31m}[m
    [31m// You are working with copies by default[m
    [32mint[m[31m[][m fE1 [31m=[m [31m[[m[35m1[m[31m,[m[35m2[m[31m,[m[35m3[m[31m,[m[35m4[m[31m];[m
    [01;34mforeach[m[31m([mx[31m;[m fE1[31m)[m[31m{[m
      x [31m*=[m [35m2[m[31m;[m
    [31m}[m
    [01;30mwriteln[m[31m([mfE1[31m);[m
    [31m// Using reference changes values[m
    [01;34mforeach[m[31m([m[01;34mref[m x[31m;[m fE1[31m)[m[31m{[m
      x [31m*=[m [35m2[m[31m;[m
    [31m}[m
    [01;30mwriteln[m[31m([mfE1[31m);[m

    [01;30mwriteln[m[31m([m[31m"Forward"[m[31m);[m
    [01;34mforeach[m[31m([melem[31m;[mitems[31m)[melem[31m.[mwriteln[31m;[m

    [01;30mwriteln[m[31m([m[31m"backward"[m[31m);[m
    [01;34mforeach_reverse[m[31m([melem[31m;[mitems[31m)[melem[31m.[mwriteln[31m;[m

    [31m//foreach copies elements, use ref to change.[m
    [01;34mforeach[m[31m([m[01;34mref[m elem[31m;[mitems[31m)[melem[31m*=[m[35m2[m[31m;[m

    [01;34mforeach[m[31m([mindex[31m,[melem[31m;[mitems[31m)[m[31m{[m items[31m[[mindex[31m].[mwriteln[31m;[m elem[31m.[mwriteln[31m;[m [31m}[m

    [01;34mforeach[m[31m([mindex[31m;[m[35m0[m[31m..[mitems[31m.[mlength[31m)[mitems[31m[[mindex[31m].[mwriteln[31m;[m

    [32mint[m [31m[[mstring[31m][m ages[31m=[[m[31m"Alain"[m[31m:[m[35m5[m[31m,[m[31m"Eddy"[m[31m:[m[35m2[m[31m];[m
    [01;34mforeach[m[31m([m[37mstring[m akey[31m,[m[32mint[m aval [31m;[m ages[31m)[m 
	    [01;30mwritefln[m[31m([m[31m"%s : %s"[m[31m,[makey[31m,[maval[31m);[m

    [31m//foreach string[m
    [01;34mforeach[m [31m([mac[31m;[m[31m"hello"[m[31m)[m
        [31m{[m[01;30mwriteln[m[31m([mac[31m);[m[31m}[m

[31m}[m[31m//main[m

