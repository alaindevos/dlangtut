import std.stdio:writeln,writefln;

void main () { 

// Logical Operators : && || !
 	writeln("true || false = ", (true || false));//true
	writeln("!true = ", (!true));//false
    bool myequal = ( 2 == 2);
    bool my_xor = (true ^ false);
// Relational Operators : > < >= <= == !=

//TERNARY
	// The ternary operator returns the 1st value
	// when the condition is true and the 2nd
	// otherwise
    int age=14;
	bool canVote = (age >= 18) ? true : false;
	writeln("Can Vote : ",canVote);//false

	string s= (1==1) ? "gelijk" : "ongelijk";
	writeln(s);//gelijk
	
	string isfour = (2+2==4) ? "Isfour" : "Is not four";
	writeln(isfour);//Isfour

//IF-ELSE	
	if ((age >= 5) && (age <= 6)){
		writeln("Go to Kindergarten");
	} else if ((age >= 7) && (age <= 13)){
		writeln("Go to Middle School");
	} else if ((age >= 14) && (age <= 18)){
		writeln("Go to High School");// !!!!!!!!!!!!!!!!
	} else {
		writeln("Stay Home");
	}

	int a = 10; 
	if     ( a == 9  ) writeln("a is  9" );
	else if( a == 10 ) writeln("a is 10" );// !!!!!!!!!!!!!!!!!!!
	else               writeln("a is not 9 or 10");


	if (1>2)
		writeln("A");
	else if (2>3)
		writeln("B");
	else
		writeln("C");//!!!!!!!!!!!!!!!!!!!!!!!!!

//SWITCH
	// Switch works great with limited options
	string lang = "France";
	switch(lang){
		case "Chile": case "Cuba":
			writeln("Hola");
			break;
		case "France":
			writeln("Bonjour");//!!!!!!!!!!!!!!!!!!!
			// Use goto to check the next
      goto case;
		case "Japan":
			writeln("Konnichiwa");//!!!!!!!!!!!!!!!!!!
			break;
		default:
			writeln("Hello");
		}
	// You can also use ranges
	int anAge = 7;
	switch(anAge){
		case 7: .. case 13:
			writeln("Go to middle school");//!!!!!!!!!!!!!!!!
			break;
		default:
			writeln("Somewhere else");
	}

	int expression=1;	
	switch (expression) {
		case 0:  writeln("0");break;
		case 1:  writeln("1");break;//!!!!!!!!!!!!!!!!!
    		default: writeln("other");break;
    }

   	int xx=2;
	switch(xx) {
		case 1:
			writeln("");
			break;
		case 2:
			writeln("");
			break;
		case 3:
		case 4:
			writeln("");
			break;
		default:
			writeln("");
			break;
	}
	

	
//GOTO-BREAK
	int b=1;
	goto MYLABEL;
	//b=2;

MYLABEL:
	writeln(b);//1
	

	int [] items=[1,2,3];
	int t=0;


MYBREAK:
	while(true)
	{
	 if(t==items.length)break MYBREAK;
	 items[t++].writeln; //1 2 3
	}

}//main

