import std.stdio:writefln;
import object: destroy;
import core.memory: GC;
import core.stdc.stdlib: malloc,free;
import std.typecons;

class C{
	int[1000] x;
}

void dofun()
{
   int[] i=new int[10000];
   writefln("%12x",&i);
   GC.free(GC.addrOf(i.ptr));   
   C c=new C;
   writefln("%12x",&c.x);
   GC.free(GC.addrOf(cast(void *)c));   
}
int main(){
   dofun();
   int[] i=new int[10000];
   writefln("%12x",&i);
   GC.free(GC.addrOf(i.ptr));   
   C c=new C;
   writefln("%12x",&c.x);
   GC.free(GC.addrOf(cast(void *)c));   
   return 0;

}
