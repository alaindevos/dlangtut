import std.stdio: writeln;

class C{
	int[] i=null;
	this(){
		writeln("Allocate heap");
		i=new int[10000];
		writeln(typeid(typeof(i)));
		writeln(typeid(typeof(i.ptr)));
		i[9000]=5;
	}
	~this(){
		writeln("Free heap");
		import object: destroy;
		import core.memory: GC;
		i=null; // But How to force GC free ?
	};
}

struct S{
	C c;
	@disable this();
	this(int dummy){c=new C;}
}

void main(){
	enum _=0;
	writeln(S(_).c.i[9000]);
}

