// ---- FUNCTIONS ----------------------------------------------------

import std.algorithm: find,sort;
import std.algorithm.iteration: filter;
import std.stdio: writeln;
import std.meta: AliasSeq;


void donothing(){
}

int getSum3(int x, int y){
  return x + y;
}
double getSum3(double x, double y){
  return x + y;
}

//PURE: Pure functions cannot directly access global or static mutable state.
//NOTHROW:Nothrow functions can only throw exceptions derived from class Error. 
//NOGC:functions do not allocate memory on the GC heap
//SAFE:No unsafe casting
double myfun(int x) pure nothrow @nogc @safe{
	return x/10.0;
}

struct Astruct{
	void func(){writeln("Hallo");}
}

//*********************************************************************
//*********************************************************************
void main(string[] args){
// Function definitions follow the format returnType functionName(parameterType parameterName)

int getSum(int x, int y){
return x + y;
}
writeln("5 + 4 = ", getSum(5,4));//9

// You can overload functions by having different parameter types
writeln("5.4 + 4.5 = ", getSum3(5.4,4.5));//9.9


// Receive a variable number of arguments
int getSum2(A...)(A args){
	int sum = 0;
	foreach(x; args){
		sum += x;
	}
	return sum;
}
writeln("Sum : ", getSum2(1,2,3,4));//10

// Recursion : Function calls self
int fact(int num){
	// Must have a condition were we don't call for the function to execute
	if(num == 1){
		return 1;
	} else {
	int result = num * fact(num - 1);
	return result;
	}
}
writeln("Fact 4 : ", fact(4));//24
// 1st : result = 4 * factorial(3) = 4 * 6 = 24
// 2nd : result = 3 * factorial(2) = 3 * 2 = 6
// 3rd : result = 2 * factorial(1) = 2 * 1 = 2

// IN-OUT-INOUT----------------------------------------------------------

// IN parameter-IN implies const but is better arguments marked with in can't be changed.The same is true with const
void randFunc(in int x, const int y){
	// Will cause error -> x = 1;
}
void infun(in int x)
	{// x=x+1 ; // error 
	}
infun(1);


// OUT PARAMETER , cfr pass by reference---------------------------------
// But out parameter is initialised to .init at entry out returns values to parameters
void randFun2(out int f2){
	f2 = 100;
}
int f2 = 5;
randFun2(f2);
writeln("f2 : ", f2);//100
void outfun(out int x)
	{x=5;
	}
int ff=0;
outfun(ff);
writeln(ff); //5

// INOUT PARAMETER : works for same types on lhs and rhs. Sort of template
inout doit1(inout int x){
	return x;
}
int x1=1;
writeln("inout1:",typeid(doit1(x1)));  //int
const int x2=1;
writeln("inout2:",typeid(doit1(x2)));  //const(int)
immutable int x3=1;
writeln("inout3:",typeid(doit1(x3)));  //immutable(int)

//Property---------------------------------------------------------------
void printme() @property
	{writeln("Hallo");}
printme;//Hallo
printme;//Hallo

//UFCS-------------------------------------------------------------------
"Hello World".writeln;//Hello World

cast(void) myfun(10);
cast(void) 10.myfun; // function in module scope !!!!!!!!!!


//PURE---------------------------------------------------------------------
//D, a function is considered pure if returning a result is
//its only effect and the result depends only on the function's arguments.
//The nothrow attribute specified with a function conveys the information 
//that that function will never th row an exception.
int iampure(int x) nothrow pure
	{return x;}

//Default arguments----------------------------------------------------------
int addme(int x=1,int y=2){
	return x+y;
}
addme(3,4);
addme(3);
addme();
addme;

//No arguments-------------------------------------------------------------
int donothing(){
	return 1;
}
donothing();
donothing;

//Lazy parameter------------------------------------------------------------

int getInt() {
    writeln("Entered getInt");
    return 10;
}

void normalParam(int x) {
    writeln("Entered normalParam");
    writeln(x);
}


void lazyParam(lazy int x) {
    writeln("Entered lazyParam.");
    writeln(x);
}

normalParam(getInt()); // Prints getInt,normalParam
lazyParam(getInt());   // Prints lazyParam,getInt

//AliasSeq & function-------------------------------------------------------


void foo(int i, string s, double d) {
    writeln("foo is called with", i," "," ", s," ",d);
}

alias arguments = AliasSeq!(1, "hello", 2.5);
foo(arguments);// foo is called with1  hello 2.5

//AliasSeq & array
alias elements = AliasSeq!(1, 2, 3, 4);
auto arr = [ elements ];


//VAL & REF STUFF******************************************************************

// variable length array as parameter
void doubleme(int[] x){
foreach (ref rx;x)rx=rx*2;
}
int[]  ai=[1,2,3];
doubleme(ai);
writeln(ai[0]);//2

// Variables changed in functions don't effect values outside of it unless you use ref
void changeMe(ref int f1){
	f1 = 5;
}
int f1 = 10;
changeMe(f1);
writeln("f1 : ", f1);//5


// Return multiple values with an array
int[] getNext2(int x){
	int[] next2 = [x+1, x+2];
	return next2;
}
int[] next2 = getNext2(1);
writeln(next2);//[2,3]

//Pass by reference
void changecaller(ref int x)
	{x=x+1;}
int xxx=1;
changecaller(xxx);
writeln(xxx);	//2

//Pass by reference
ref int inc2(ref int x) return pure nothrow @nogc @safe{
	++x;
	return x;
}
int x=0;
inc2(inc2(x));
writeln(x);	 //2

//Return by reference
int y=3;
ref int returny(){
	return y;
}
returny()=7;
writeln(y); //7


//Byval : pointer and length are copied
void appendbyval(int []arr){
	arr[0]=1;
	arr~=2;
}
int[] aaa=[-1,-2,-3];
appendbyval(aaa);
writeln(aaa.length); //3
writeln(aaa[0]);     //1


//Byref : No copy
void appendbyref(ref int [] arr){
	arr[0]=1;
	arr~=2;
}
int[] bbb=[-1,-2,-3];
appendbyref(bbb);
writeln(bbb.length); //4
writeln(bbb[0]);     //1

//Fixed length array
//Byval
int[3] ccc=[-1,-2,-3];
// appendbyref(ccc); Error !!! fixed length to variable length array
void changebyval(int [3]arr){
	arr[0]=10;
}
changebyval(ccc);
writeln(ccc[0]);  // -1 , ie no change , the array is copied !!!!!!

//Byref
void changebyref(ref int [3]arr){
	arr[0]=10;
}
changebyref(ccc);
writeln(ccc[0]);  // 10 , value changed !!!!!!!!!!!!!!!!!!!!!!!!!!!!

//Return fixed length array in variable length argument
//From stack to heap ...
int [] fromstacktoheap(){
	int[3] stackarray;
	//dup makes a heap element from a stack element
	return stackarray.dup;
	//Here the stack goes out of scope, stackarray also but not dup
}	

//Bad version
int * returnintpointer1(){
	int x=10;
//	return &x ;   //Error adres of stackelement 
	return null;
	}

//Good version
int * returnpointer2(){
	int * p=new int;     //Heap
	*p=15;
	return p;
	}

//HIGHER ORDER FUNCTIONS
//Lambda's-FIND-----------------------------------------
int [] q=[1,2,3,4,5,6];
auto b=find!
	( function bool(int x) 
		{return (x <0);}) (q);

//SORT---------------------------------------------
struct MYS {
	int x;
}
MYS [] sarr=[MYS(0),MYS(2)];
sarr.sort!( 
	(a,b) 
		{ 
		   bool ret= a.x > b.x; 
		   return ret; 
		} 
);
foreach (t;sarr)writeln(t.x);//2 0

//FILTER-----------------------------------------

int[] numbers=[1,2,3,4,5,6];
numbers.filter!(number=>number>3).writeln;//[4,5,6]


//Lambda function, return type is inferred------------------------------
//anonymous function------------------------------------------------------

auto fff=function (int i) => i+1 ;
alias myfunx=function int(int number) { return number; };

writeln(fff(3));//4

double myfunction(int x)
	{return x;}

alias ftype=double delegate(int);

double myfunctionfunction(ftype f)
	{return f(0);}
	
myfunctionfunction(&myfunction);	

int function(int) myfuny=(int x)=>x+1;
writeln(typeid(myfuny));//int function(int)*

//*************************************************************************
//Function-pointers--------------------------------------------------------
//Default initialised to null, to take address use & !!!!!

Astruct AS=Astruct();
auto fp1=&AS.func;
writeln(typeid(fp1));//void delegate()
fp1();//Hallo

auto fp2=fp1.ptr;     //Pointer to struct
writeln(typeid(fp2)); //void *
auto fp3=cast(Astruct *)fp2;
(*fp3).func;//Hallo

//Function foo, returning int
//A delegate is a combination of a function pointer and the context that it should be executed in. Delegates also support closures
int delegate(double) foo1;  //Variable

int testfunction(double a){
	return 0;
	}

foo1=&testfunction;
foo1(3);            //call testfunction
//Alias-type
alias fp= int delegate(double);
fp foo2;
foo2=&testfunction;
foo2(3);

//Function bar, returning function pointer.
int function(double) function(string) bar;

writeln("--------------------");
//Function pointers
static double Gfunction(int x)
    {return x/10.0;}
writeln("Gfunction",typeid(typeof(Gfunction))); //OUTPUT:"double function(int) pure nothrow @nogc @safe"
double Lfunction(int x)
    {return x/20.0;}
writeln("Lfunction",typeid(typeof(Lfunction))); //OUTPUT:"double function(int) pure nothrow @nogc @safe"

//Function pointers. Static function has no context or this
alias gfunction = double function(int);
gfunction g = & Gfunction;
writeln(g(100),typeid(typeof(g))); //OUTPUT:  10 double function(int)*
double function(int) F = function double(int x) {return x/25.0;};
writeln(F(100),typeid(typeof(F))); //OUTPUT:  4 double function(int)*

//Function pointers. Local function has context & this => delegate
alias lfunction = double delegate(int);
lfunction l = & Lfunction;
writeln(l(100),typeid(typeof(l))); //OUTPUT: 5 double delegate(int)
int c=2;
double delegate(int)   D = delegate double(int x) {return c*x/50.0;};
writeln(D(100),typeid(typeof(D))); //OUTPUT: 4 double delegate(int) 
writeln("--------------------");

}//main
