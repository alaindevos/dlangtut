[31m//OpApply is the iterator, alternative is use of the range interface[m
[01;34mimport[m std[31m.[mstdio[31m:[mwriteln[31m;[m

[01;34mstruct[m [37mIterateMe[m [31m{[m
	[01;34menum[m [37mlen[m [31m=[m [35m10[m[31m;[m
	[32mint[m[31m[[mlen[31m][m values[31m;[m
	[01;34mvoid[m [01;30minitialize[m[31m()[m [31m{[m
		[01;34mforeach[m[31m([mi[31m;[m [35m0[m[31m..[mlen[31m)[m [31m{[m
			values[31m[[mi[31m][m [31m=[m i[31m;[m
		[31m}[m[31m//forearch[m
	[31m}[m[31m//initialise[m

	[31m//Iterator[m
	[32mint[m [01;30mopApply[m[31m([m[32mint[m [01;34mdelegate[m[31m([m[01;34mref[m [32mint[m[31m)[m dg[31m)[m [31m{[m
		[32mint[m result[31m;[m
		[01;34mforeach[m[31m([m[01;34mref[m v[31m;[m values[31m)[m [31m{[m
			result [31m=[m [01;30mdg[m[31m([mv[31m);[m
			[01;34mif[m[31m([mresult[31m)[m
				[01;34mbreak[m[31m;[m
		[31m}[m[31m//for[m
		[01;34mreturn[m result[31m;[m
	[31m}[m[31m//opAplply[m
[31m}[m[31m//IterateMe[m

[01;34mstruct[m [37mIterateMe2[m [31m{[m
	[01;34menum[m [37mlen[m [31m=[m [35m10[m[31m;[m
	[32mfloat[m[31m[[mlen[31m][m values[31m;[m
	[01;34mvoid[m [01;30minitialize[m[31m()[m [31m{[m	
		[01;34mforeach[m[31m([mi[31m;[m [35m0[m[31m..[mlen[31m)[m 
			[31m{[mvalues[31m[[mi[31m][m [31m=[m i[31m;[m[31m}[m
	[31m}[m[31m//initialize[m
	[32mint[m [01;30mopApply[m[31m([m[32mint[m [01;34mdelegate[m[31m([m[01;34mref[m [32mfloat[m[31m)[m dg[31m)[m [31m{[m
		[32mint[m result[31m;[m
		[01;34mforeach[m[31m([m[01;34mref[m v[31m;[m values[31m)[m [31m{[m
			result [31m=[m [01;30mdg[m[31m([mv[31m);[m
			[01;34mif[m[31m([mresult[31m)[m
				[31m{[m[01;30mwriteln[m[31m([m[31m"A"[m[31m);[m[01;34mbreak[m[31m;[m[31m}[m
		[31m}[m[31m//for[m
		[01;30mwriteln[m[31m([m[31m"B"[m[31m);[m
		[01;34mreturn[m result[31m;[m
	[31m}[m[31m//opapply[m
[31m}[m[31m//iterateme[m

[01;34mvoid[m [01;30mmain[m[31m()[m [31m{[m
	[37mIterateMe[m im[31m;[m
	im[31m.[m[01;30minitialize[m[31m();[m
	[01;34mforeach[m[31m([mi[31m;[m im[31m)[m
		[01;30mwriteln[m[31m([mi[31m);[m

	[37mIterateMe2[m im2[31m;[m
	im2[31m.[m[01;30minitialize[m[31m();[m
	[01;34mforeach[m[31m([mi[31m;[m im2[31m)[m
		[01;30mwriteln[m[31m([mi[31m);[m
[31m}[m[31m//main[m
