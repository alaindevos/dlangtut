//OpApply is the iterator, alternative is use of the range interface
import std.stdio:writeln;

struct IterateMe {
	enum len = 5;
	int[len] values;
	void initialize() {
		foreach(i; 0..len) {
			values[i] = i;
		}//forearch
	}//initialise

	//Iterator
	int opApply(int delegate(ref int) dg) {
		int result;
		foreach(ref v; values) {
			result = dg(v);
			if(result)
				break;
		}//for
		return result;
	}//opAplply
}//IterateMe

struct IterateMe2 {
	enum len = 5;
	float[len] values;
	void initialize() {	
		foreach(i; 0..len) 
			{values[i] = i;}
	}//initialize
	int opApply(int delegate(ref float) dg) {
		int result;
		foreach(ref v; values) {
			result = dg(v);
			if(result)
				{writeln("A");break;}
		}//for
		writeln("B");
		return result;
	}//opapply
}//iterateme

void main() {
	IterateMe im;
	im.initialize();
	foreach(i; im)
		writeln(i);//0,1,2,3,4

	IterateMe2 im2;
	im2.initialize();
	foreach(i; im2)
		writeln(i);//0,1,2,3,4,B
}//main
