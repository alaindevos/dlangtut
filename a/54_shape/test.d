import std.stdio;

// This defines what we can do with a Shape:
interface Shape {
  void draw();
}

// This type is now a 'class' that implements Shape.
class Circle : Shape {
  float radius;

  // Classes require constructors; so here is one:
  this (float radius) {
    this.radius = radius;
  }

  // No parameter needed. This function always executes on
  // 'this' object.
  void draw() {
    writeln("This circle's radius is ", radius);
  }
}

// Similarly, a class
class Rectangle : Shape {
  float width;
  float height;

  this(float width, float height) {
    this.width = width;
    this.height = height;
  }

  void draw() {
    writefln!"This rectangle's dimensions are %sx%s."(width, height);
  }
}

// Here is the promise of polymorphism: This function takes
// a Shape but the special drawing of each shape type is
// handled automatically.
void use(Shape shape) {
  shape.draw();
}

void main() {
  // Class hierarchies allow putting different types into
  // the same array:
  Shape[] shapes;

  // Let's populate with alternating circles and rectangles
  foreach (i; 1 .. 10) {
    if  (i % 2) {
      shapes ~= new Circle(i);

    } else {
      shapes ~= new Rectangle(i, i * 2);
    }
  }

  // And finally let's use them
  foreach (shape; shapes) {
    use(shape);
  }
}
