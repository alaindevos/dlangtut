import std.stdio:writeln,writefln;
import std.typecons: tuple;

void main () { 

int [] items=[1,2,3];
int t;

//FOR-------------------------------------------
// For loop
    for(int i = 0; i < 5; i++){
      writeln(i);
    }

	int c=1;
	for(;;){
		c=c+1;
		if (c==10) break;
	}
	writeln(c);

	for(int t2=0;t2<items.length;++t2)
		items[t2].writeln;


//WHILE
    // While executes as long as a condition
    // is true
    int wI = 0;
		while (wI < 20) {
			// Only print even numbers
			if(wI % 2 == 0) {
				writeln(wI);
				wI++;
				// Jump back to the beginning of loop
				continue;
			}
			if(wI >= 10) {
				// Stop looping
				break;
			}
			wI++;
		}
		
	while(t<items.length)
		items[t++].writeln;

//DO-WHILE
	// Do While always executes once
	// int secretNum = 7;
	// int guess = 0;
	// do {
		// 	write("Guess : ");
		// 	readf("%d\n", &guess);
		// }while(secretNum != guess);
		// writeln("You guessed it");

	t=0;
	do{
		items[t++].writeln;
		}	while(t<items.length);


//FOREACH
	// foreach is more commonly used
	// for containers
	int[] a6 = [1,2,3,4];
	foreach(x; a6){
		writeln(x);
	}
	// Work with ranges
	foreach(x; 5..10){
		writeln(x);
	}

	// Associative Arrays foreach loop
    double[string] aA =
    ["A" : 1,
    "B" : 2];
    // Keys and values
    foreach(k, v; aA){
      writeln(k, " ", v);
    }
    // Get both as a group
    foreach(x; aA.byKeyValue){
      writefln("%s : %s",
      x.key, x.value);
    }
    // You are working with copies by default
    int[] fE1 = [1,2,3,4];
    foreach(x; fE1){
      x *= 2;
    }
    writeln(fE1);
    // Using reference changes values
    foreach(ref x; fE1){
      x *= 2;
    }
    writeln(fE1);

    writeln("Forward");
    foreach(elem;items)elem.writeln;

    writeln("backward");
    foreach_reverse(elem;items)elem.writeln;

    //foreach copies elements, use ref to change.
    foreach(ref elem;items)elem*=2;

    foreach(index,elem;items){ items[index].writeln; elem.writeln; }

    foreach(index;0..items.length)items[index].writeln;

    int [string] ages=["Alain":5,"Eddy":2];
    foreach(string akey,int aval ; ages) 
	    writefln("%s : %s",akey,aval);

    //foreach string
    foreach (ac;"hello")
        {writeln(ac);}

    //foreach tuple
    auto tup = tuple(42, "hello", 1.5);
    foreach (i, member; tup) {
        writefln("%s: %s", i, member);
}

}//main

