import std.stdio:writeln,writefln;

void main(){

import std.range: iota,drop,stride,retro,chain,roundRobin,empty; 

//------------------------------------------------------------------------------------
//iota Consequtive sequence of integers

foreach(i; iota(10)) writeln(i);
foreach(i; iota(1, 11)) writeln(i);
foreach(i; iota(2, 21, 2)) writeln(i); // Prints all even numbers from 2 - 20

auto r = iota(1, 21).drop(10);
foreach(i; r) writeln(i);

auto r1 = iota(21).stride(2); //iterates its elements according to a step value
foreach(i; r) writeln(i);

auto r2 = iota(1, 21).retro(); //backwards
foreach(i; r2) writeln(i);

auto r3 = chain(iota(10), iota(10, 20), iota(20, 30)); //Concatinate
foreach(i; r3) writeln(i);

auto rr = roundRobin(iota(3), iota(3, 6), iota(6, 9)); //Alternate
foreach(i; rr) writeln(i);

//------------------------------------------------------------------------------------
//A recurrence equation recursively defines a sequence of numbers. Given an initial
//number to begin the sequence, subsequent numbers are each defined as a function
//of the preceding numbers.
import std.range: recurrence,take;
auto r5 = recurrence!("a[n-1] + a[n-2]")(0, 1).take(20);
auto r6 = recurrence!((a,n) => a[n-1] + 2)(2).take(10);

//------------------------------------------------------------------------------------
//A sequence. This function is similar to recurrence, but the numbers are generated differently.
//Instead of an equation where the value of n depends on the previous values,
//sequence uses a closed form expression, where the nth value is a function of the
//initial value and n itself.
import std.range : sequence, take, dropOne;
auto ra = sequence!("n*2").dropOne.take(10);

import std.range: transposed;
auto arr = [[1, 2, 3], [4, 5, 6]];
auto rb = transposed(arr);
foreach(a; rb) writeln(a);

//-------------------------------------------------------------------------------------------
import std.range : zip,lockstep;
import std.algorithm : filter;
auto rc = zip(iota(1, 11), iota(11, 21)).filter!(t => (t[0] & 1) == 0); //Returns an array of tuples
foreach(i; rc) writeln(i);

//-Lockstep for iterator
foreach(i, x, y; lockstep(iota(1, 11), iota(11, 21))) writefln("%s. x = %s and j = %s", i, x, y);

//-------------------------------------------------------------------------------------------
import std.algorithm: equal,cmp,group,map,copy,fill,remove,find,count,sort,reduce,any;

bool eee=equal(iota(51).take(20), iota(20));
int order=cmp("Alain","Eddy");

// Group,looks at the number of times an element appears
//consecutively in sequence, and returns a range containing a tuple of each element
//and the number of times it appears in the sequence
auto arra = [1, 1, 1, 1, 2, 3, 3, 6, 6, 4, 3, 3, 3, 3];
auto r4 = arra.group();
foreach(val, countv; r4) writefln("%s appears %s times", val, countv);

//map applies the given callable to each element
auto rp = iota(1, 21).map!(x => x * 2);
foreach(n; rp) writeln(n);

//reduce, accumulator = fun(accumulator, e).
int sum=iota(6).reduce!("a + b");

int[20] sink;
auto r9 = iota(10).copy(sink[]);

int[20] sink2;
fill(sink2[], 100);
foreach(n; sink2)writeln(n);

auto arr2 = [1,2,3,4,5,6,7,8,9];
arr2 = arr2.remove(2,3);
writeln(arr);

auto s = "Like Frankie said I did it my way.";
auto r8 = s.find("Frankie");
if(!r8.empty) writeln(r8); // s contains "Frankie"

auto sm = "Mike Parker";
ulong cs=sm.count("ke");

//any , true if at least one satifies predicate
bool b=iota(50).any!("(a * 2) > 50");

auto ss=["Eddy","Alain"].sort();

//------------------------------------------------------------
import std.array: join;
auto j=["Alain","Eddy"].join("|");


}
