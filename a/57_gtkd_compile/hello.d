import std.stdio;
import gtk.MainWindow;
import gtk.Main;
import gtk.Widget;

void quitApp(){
	writeln("Bye.");
	Main.quit();
} // quitApp()

void main(string[] args){
	Main.init(args);
	MainWindow testRigWindow = new MainWindow("Test Rig");
	testRigWindow.addOnDestroy(delegate void(Widget w) { quitApp(); } );
	writeln("Hello GtkD Imperative");
	testRigWindow.showAll();
	Main.run();
} // main()
