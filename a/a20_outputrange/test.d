import std.range;
import std.stdio;

struct MyHexDump{

	void put(in void[] data){
		auto data2=cast(const ubyte[])data;
		foreach (b;data2) writef("%02x ",b);
	}
	~this(){writeln();}
}

void main(){
	MyHexDump d;
	d.put("Hello World");
}
