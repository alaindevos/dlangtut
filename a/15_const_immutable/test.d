/*
CONST : Hint compiler, promise not to change
	  : It can not be changed by the current code , well by other code
IMMUTABLE : In memory page marked as read-only

CONST&IMMUTABLE are transitive to content.

const parameters are the ones that functions do not modify. Both mutable
and immutable variables can be passed as arguments of const parameters.
*/

import std.stdio;
import std.stdio: writeln;

void doit1(const int []a){
//a[0]='0' :Not allowed
}

void doit2(immutable int []a){
}

void main(){

const int   c5=1;
const (int) c6=1;//idem
const       c7=1;//idem

//Conversion----------------------------------------

//Const is a bridge between the 3.

immutable int    xi=10;

int              xm=xi;  // Unqualified
const(int)*     cpm=&xm; // Unqualified to const

immutable(int)* ipx=&xi;
const(int)*     cpx=ipx; // Immutable to const


//ARRAY---------------------------------------------
const(int)[] t; // Mutable array, const data

t ~= [3,2,1]; // OK: mutable array
t.length = 30; // OK: mutable array

const(int[]) u; // const array, const data
const int[] v; // const array, const data


int [] a=new int[10];
immutable int [] b=new immutable int[10];
doit1(a); //OK : mutable->const
doit2(b); //OK : immutable ->immutable
//  doit2(a); //Not allowed

//Conversion from Fixed-Size arry to dynamic size array
immutable(int) forever = 42;
alias immutable(int) stableint;
stableint forever2=42;

//Type is automatic induced
immutable pi=3.14;

int a1=42;
immutable(int)a2=a1; // OK no indirection
int a3=a2;           // OK no indirection

int []b1=[42];
//immutable(int[]) b2=b1;  Error
//int []b3=b2;  Error


//Mutatable array to const data
const(int)[] m;
//m[0]=1  //error const data
m~=[1,2,3];
m.length=10;


//Strings,chars-----------------------------------------------------

char[] s5="Hello".dup; //copy immutable to mutable
string s6=s5.idup;     //copy muttable to immutable

string process1(string input);
immutable(char)[] process2(immutable(char)[] input);

string s;
writeln(typeid(typeof(s))); // Prints immutable(char)[]

char [] r=['1','2','3'];
r[1]='x';
r=cast(char [])"Two";
writeln(typeid(typeof(r)));//char[] 

immutable(char) []t="One";
t="Tree";
writeln(typeid(typeof(t)));//immutable(char)[]
//This is a mutable reference to immutable chars

immutable(char[]) c="One";
writeln(typeid(typeof(c)));//immutable(immutable(char)[])
//This is an immutable reference to immutable chars

immutable(char)[3] d="abc";
//Fixed length string

immutable(char) [3] e="abc";
immutable(char) []  f=e;

//Pointers----------------------------------------------------------

const (int)*  mtoc; // Mutable pointer to const data
const (int*)  ctoc1; // Const pointer to const data
const int*    ctoc2; // Const pointer to const data , do not use this form

const(int) * q5= (new int [2]).ptr;  //Mutable pointer to const data
const(int *) q6= (new int [2]).ptr;  //Const pointer to const data

//Convert immutable pointer to const pointer is allowed
immutable int x=10;
immutable(int)* ipx=&x;
const(int)* cpx=ipx;       //Conversion
cpx=&x;                    //Conversion


//Struct----------------------------------------------------------

//Transitivity, to prevent side effects in functional programming
struct Point { int x,y;}
auto origin=immutable(Point)(0,0);
// origin.x=5 // Not-allowed



//Class-----------------------------------------------------------
class AAA{
	//Regular method
	int[] fun1();
	//Immutable this
	int[] fun2() immutable;
	//Immutable this returning immutable object
	immutable(int[]) fun3() immutable;
	//immutable scope
	immutable {
		int fun3();
	}
	//immutable label
	immutable:
		int fun4();
}//Class A

//Storage class
//immutable methods can only be used on immutable objects
//const methods can only be used on const objects
class C{
	void f(){}
	void g() immutable {}
	void h() const {}
	void i() inout {}
}
auto c1=new C;
c1.f();
//c1.g(); Error
auto c2=new immutable(C) ;
//c2.f(); error
c2.g();
c2.h();
c2.i();

class D {
	int a;
	int[] b;
	//immutable constructor
	this() {
		a=5;
		b=[1,2,3];
		//fun(); not allowed !!!!!!
	}
	void fun() immutable{}
}

}//main

