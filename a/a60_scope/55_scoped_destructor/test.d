import std.stdio;    //writeln
import std.typecons; //scoped

@safe pure class C {
   @safe pure immutable int mydata;
   @safe pure this(scope immutable int d) {mydata=d;}
   @safe ~this() {writeln("Hi");}
}

void main() {
  {
    auto c = scoped!C(42);
    writeln(c.mydata);
  }
  writeln("after scope");
}
