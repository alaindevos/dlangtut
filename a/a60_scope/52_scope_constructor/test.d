import std.stdio:writefln;
import object: destroy;
import core.memory: GC;
import core.stdc.stdlib: malloc,free;
import std.typecons;

class C {
     int * pa;
     int [] a;
	// Constructor
	this() {writefln("Called constructor");
           pa=cast(int *)malloc(1000*int.sizeof);
           a=pa[0..1000];
          }
	
}

void dofun()
{
   scope x=new C;
   x.a[3]=5;
   writefln("%12x",&x);

}
int main(){
   dofun();
   dofun();
   return 0;

}
