class C {
	@nogc this(){}
	@nogc this(int dummy){};
	@nogc int[3] fixarr=new int[3];
}//C

@nogc void myfun(){
	int a;
	scope c = new C();
	scope c2 = new C(5);
	scope int[] i=[1,2,3];
}//myfun

void main(){
	myfun();
}//main

