import std.stdio : writeln;
import std.algorithm : map;
import std.array : array;

void main(){
    int[] arr1=[1,2,3,4];
    int delegate(int) square= x=>x*x;
    alias square2=square;
    int[] arr2=arr1.map!(square2).array;
    writeln(arr1);
    writeln(arr2);
}
