import std.stdio: writeln;
import std.concurrency: thisTid;
import core.thread.osthread: Thread;
import core.time: seconds;
import std.parallelism: totalCPUs,parallel;

void main(){
	
//concurrency,parallelism ,fibers
     // ---- THREADS ----
    // A thread is a block of code that is expected
		// to execute while other blocks of code execute
 
    // Let's run different blocks pausing as we go
    // void doStuff(int x){
    //   writeln(x, " start");
    //   Thread.sleep(1.seconds);
    //   writeln(x, " stops");
    // }
    //
    // auto now1 = Clock.currTime(UTC());
    // auto sS = now1.second;
    //
    // doStuff(1);
    // doStuff(2);
    // doStuff(3);
    //
    // auto now2 = Clock.currTime(UTC());
    // auto sS2 = now2.second;
    // writeln("Seconds : ", sS2 - sS);
    class Worker{
      int id;
      this(int id){
        this.id = id;
      }
      void work(){
        // Get thread id
        writeln(id, " : ", thisTid);
        writeln("Thread ", id, " starts");
        Thread.sleep(1.seconds);
        writeln("Thread ", id, " ends");
      }
    }
    // Total cores
    writeln("Cores : ", totalCPUs);
    auto workers = [new Worker(0), new Worker(1),
    new Worker(2)];
    // Parallel will execute the functions in parallel
    foreach(w; parallel(workers)){
      w.work();
    }
}
