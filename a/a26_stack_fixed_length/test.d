import std.stdio;

void main(){
	struct MyStack {
		float [10] buf;
		size_t currentPosition;
		bool isEmpty(){return currentPosition==0;}
		void push(float item){
			buf[currentPosition]=item;
			currentPosition++;
		}
		float pop(){
			--currentPosition;
			return(buf[currentPosition]);
		}
	}
	MyStack s;
	s.push(1);
	s.push(2);
	writeln(s.pop());
	writeln(s.pop());
}
