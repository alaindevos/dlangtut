import std.stdio:writeln;
import std.range:inputRangeObject;

void main(){
	auto r=inputRangeObject([1,2,3]);
	foreach(int item; r) writeln(item);
}
